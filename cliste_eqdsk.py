'''
--------------
 cliste_eqdsk
--------------

Read CLISTE equilibria from Cliste "eqdsk" file

Based on "cliste_reader.py" by micdu
- class made compatible to "sf..." structure 
- multiple equilibria from one file (for FP equilibrium database)

Created on Thu Nov 23 08:00:00 2023

@author: micdu, wls
'''

import os
import numpy as np

import matplotlib.pyplot as plt
import inspect, os
module_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
print ("sfliste path: ", module_path)
from cliste_eqres import cliste_eqres
from utils.div_scoord import s_coord

# nominal coil data (to interpret CLISTE PF currents)
NamN = np.array(['IV1o', 'IV1u', 'IV2o', 'IV2u', 'IV3o', 'IV3u', 'Ipslo', 'Ipslu', 'ICoio', 'ICoIu', 'IOH2o', 'IOH2u', 'IOH'])
Rcn =  np.array([1.646,  1.646,  3.128,  3.128,  3.599,  3.599,   2.103,   2.021,    2.51,    2.51,  0.661,   0.661,   0.375])
Zcn =  np.array([2.358, -2.358,  1.681, -1.681,  0.694, -0.694,    0.72 ,  -0.72 ,  0.717,  -0.717,  2.007,  -2.007,   0.   ])
Nn  =  np.array([93,    93,      86,     86,     28,     28,      1,       1,       5,       5,      81,      81,      510  ])

# z tolerance around each coil
dZcn = 0.05*np.abs(Zcn)
Zcn_min = Zcn - dZcn
Zcn_max = Zcn + dZcn      


# ----------------------------------------------------------------------------------------------
class cliste_eqdsk(object):
   '''
      class to read CLISTE "eqdsk" file
   '''

   def __init__ (self, fn, fn_eqres=None, n_equil=0, shot=None,Plot=False):
      '''
        read CLISTE eqdsk file
        fn:   path to cliste "eqdsk" file
        nequil:     0 to autodetect number of equilibria
                    >0 specifiy number of equilibria to read from eqdsk file
        shot:       preset shot number for each equilibrium,
                    if shot=='None' then use shot number in eqdsk file
      '''

      #initiate lower div class
      scoord_low = s_coord(div='Lower')
      scoord_upp = s_coord(div='Upper')
      # we haven't yet read, let alone selected any equilibria
      self.eqtindx = None

      # count number of equilibria in file if not given as an argument
      # N.B. CLISTE eqdsk has no header giving this information
      #  equilibria are just padded to one another
      if n_equil==0:
         f=open(fn, 'r')
         n_equil = sum(1 for line in f if 'shot' in line)
         print("Number of equilibria = %d" % n_equil)
         f.close()
      self.n_equil = n_equil

      self.vars={}
      if shot is not None:
         self.shot = shot
         self.vars['shot'] = np.repeat(shot, n_equil)
      else:
         self.shot = 0
         
      self.eqres = cliste_eqres(fn_eqres, n_equil=self.n_equil)
      f = open(fn, 'r')
      # loop equilibria
      for self.ieq in range(self.n_equil):
         l1 = f.readline()     # skip first 'empty' line
         while not 'shot' in l1:    # synchronize
            l1 = f.readline()
         if shot is None:
            self.add_var('shot',int(l1.split()[0]))
         self.add_var('time',float(f.readline().split()[0]))
         self.add_var('Ip',float(f.readline().split()[0]))
         self.add_var('BTF',float(f.readline().split()[0]))
         # category is now (10*swiX)+ikCAT
         cat = int(f.readline().split()[0])
         self.add_var('ikCAT', cat % 10)
         self.add_var('swiX', cat // 10)
         self.add_var('NR',int(f.readline().split()[0]))
         self.NR = self.vars['NR'][0]
         self.add_var('NZ',int(f.readline().split()[0]))
         self.NZ = self.vars['NZ'][0]
         s=f.readline().split()
         self.add_var('LPF',int(s[0]))
         self.add_var('LPFp',int(s[1]))
         self.add_var('LPFe',int(s[2]))
         self.add_var('LSSQ',int(f.readline().split()[0]))

         s=f.readline()
         while not 'Rad(1:NR):' in s:
            self.add_var(s.split()[2],float(s.split()[1]))
            s=f.readline()

         self.R=np.fromfile(f,dtype=float,count=self.NR,sep=' ')
         f.readline()
         self.z=np.fromfile(f,dtype=float,count=self.NZ,sep=' ')

         if self.ieq==0:
            self.PFM=np.zeros((self.n_equil, self.NR, self.NZ))

         for iz in range(self.NZ):
            f.readline()
            self.PFM[self.ieq,:,iz]=np.fromfile(f,dtype=float,count=self.NR,sep=' ')

         # flux functions profiles
         for i in range(17):
            nam1=f.readline().split('(')[0].replace(' ','')
            v1=np.fromfile(f,dtype=float,count=self.LPFp[self.ieq],sep=' ')
            # self.add_var1d(nam1,v1)
            nam2=f.readline().split('.')[0].replace(' ','').replace('(','')
            v2=np.fromfile(f,dtype=float,count=self.LPFe[self.ieq] +1,sep=' ')
            # self.add_var1d(nam2,v2)
            # concatenate into 1 array,
            # v2 begins with [0.0, v1[-1], : ] so start with second element
            v = np.hstack( (v1, v2[2:]) )
            self.add_var1d(nam1,v)
            # print(nam1,nam2)


         # external current loops
         self.Nc=int(f.readline().split('#')[0])
         f.readline()
         Ic1=np.fromfile(f,dtype=float,count=self.Nc*5,sep=' ').reshape(-1,5)
         if self.ieq==0:   # first equilibrium: now we know #coils
            self.ncoils = Ic1.shape[0]
            self.Rc=np.zeros((self.n_equil, self.ncoils))
            self.Zc=np.zeros((self.n_equil, self.ncoils))
            self.Icd=np.zeros((self.n_equil, self.ncoils))
            self.Ice=np.zeros((self.n_equil, self.ncoils))
         self.Rc[self.ieq, :] = Ic1[:,1]
         self.Zc[self.ieq, :] = Ic1[:,2]
         self.Icd[self.ieq, :] = Ic1[:,3]  # CLD (design/measured current)
         self.Ice[self.ieq, :] = Ic1[:,4]  # CLE (equilibrium current)
         
         self.add_var('eqTEXT',int(f.readline().split()[0]))
         self.add_var('charl',int(f.readline().split()[0]))

         Faxis = self.Rz2PFM(self.vars['Rmag'][self.ieq], self.vars['Zmag'][self.ieq])
         self.add_var('Faxis', Faxis)
         Fxpu  = self.Rz2PFM(self.vars['Rxpu'][self.ieq], self.vars['Zxpu'][self.ieq])
         self.add_var('Fxpu', Fxpu)
         Fxpo  = self.Rz2PFM(self.vars['Rxpo'][self.ieq], self.vars['Zxpo'][self.ieq])
         self.add_var('Fxpo', Fxpo)

         Fx1 = self.Rz2PFM(self.eqres.vars['Rx'][self.ieq], self.eqres.vars['Zx'][self.ieq])
         self.add_var('Fx1', Fx1)
         Fx2 = self.Rz2PFM(self.eqres.vars['Rx2'][self.ieq], self.eqres.vars['Zx2'][self.ieq])
         self.add_var('Fx2', Fx2)
         
         Fil = self.Rz2PFM(self.eqres.vars['Ril'][self.ieq], self.eqres.vars['Zil'][self.ieq])
         self.add_var('Fil', Fil)
         Fol = self.Rz2PFM(self.eqres.vars['Rol'][self.ieq], self.eqres.vars['Zol'][self.ieq])
         self.add_var('Fol', Fol)

         self.descriptor = os.path.basename(fn)

         
            

         
      #new bit to read special points from corresponding eqres file
      
      
      
      s = self.read_eqscalars()
      self.Psimag = s['Psimag']
      self.Rmaxis = s['Rmaxis']
      self.zmaxis = s['zmaxis']
      self.Psibdry = s['Psibdry']

      self.vars['Runi2b'] = np.zeros(self.n_equil)
      self.vars['Zuni2b'] = np.zeros(self.n_equil)
      self.vars['Suni2b'] = np.zeros(self.n_equil)
      self.vars['Runa2b'] = np.zeros(self.n_equil)
      self.vars['Zuna2b'] = np.zeros(self.n_equil)
      self.vars['Suna2b'] = np.zeros(self.n_equil)
      
      self.vars['Robi2'] = np.zeros(self.n_equil)
      self.vars['Zobi2'] = np.zeros(self.n_equil)
      self.vars['Sobi2'] = np.zeros(self.n_equil)
      self.vars['Roba2'] = np.zeros(self.n_equil)
      self.vars['Zoba2'] = np.zeros(self.n_equil)
      self.vars['Soba2'] = np.zeros(self.n_equil)
      for self.ieq in range(self.n_equil):
         #calculate s-coordinate along lower target
         if self.vars['ikCAT'][self.ieq] == 4:
            pf_target = self.Rz2PFM(scoord_low.Rlim,scoord_low.Zlim)
            try:
               _ri,_zi,_si,_ra,_za,_sa = scoord_low.find_s(pf_target,self.Psibdry[self.ieq])
            except:
               print("An error occurred in defining SP locations, setting to 0 for equil ",self.ieq)
               _ri=_zi=_si=_ra=_za=_sa = 0.
               
            self.vars['Runi2b'][self.ieq] = _ri
            self.vars['Zuni2b'][self.ieq] = _zi
            self.vars['Suni2b'][self.ieq] = _si
            self.vars['Runa2b'][self.ieq] = _ra
            self.vars['Zuna2b'][self.ieq] = _za
            self.vars['Suna2b'][self.ieq] = _sa
         if self.vars['ikCAT'][self.ieq] == 3:
            pf_target = self.Rz2PFM(scoord_upp.Rlim,scoord_upp.Zlim)
            try:
               _ri,_zi,_si,_ra,_za,_sa = scoord_upp.find_s(pf_target,self.Psibdry[self.ieq])
            except:
               print("An error occurred in defining SP locations, setting to 0 for equil ",self.ieq)
               _ri=_zi=_si=_ra=_za=_sa = 0.
               
            self.vars['Robi2'][self.ieq] = _ri
            self.vars['Zobi2'][self.ieq] = _zi
            self.vars['Sobi2'][self.ieq] = _si
            self.vars['Roba2'][self.ieq] = _ra
            self.vars['Zoba2'][self.ieq] = _za
            self.vars['Soba2'][self.ieq] = _sa

      
      self.fluxfunctions = self.read_fluxfunctions()
      
      self.Icd = self.Icd.transpose()
      self.Ice = self.Ice.transpose()
      
      self.psin = (self.PFL - self.Psimag[:,None]) / (self.Psibdry[:,None] - self.Psimag[:,None])
      wh = np.argmin(np.abs(self.psin[int(self.n_equil/2),:] - 0.8))
      if Plot:
         npsi = self.psin.shape[1]
         fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
         surf = ax.plot_surface(np.tile(self.time,(npsi,1)).transpose(),self.psin,self.Pres)
         plt.show()


      
         
   # add a scalar value
   def add_var(self,nam,v):
      if self.ieq == 0:    # first equilibrium
         if type(v) == type(1):
            va = np.zeros(self.n_equil, dtype=np.int32)
         else:
            va = np.zeros(self.n_equil, dtype=np.float64)
         self.vars[nam]=va
         setattr(self,nam,va)
      self.vars[nam][self.ieq] = v      # insert value at the right place

   # add a 1d array
   def add_var1d(self,nam,v):
      if self.ieq == 0:    # first equilibrium
         va = np.zeros((self.n_equil, len(v)), dtype=type(v[0]))
         self.vars[nam]=va
         setattr(self,nam,va)
         # print("%s: Value len=%d  variable ntime=%d nvalues=%d  " % (nam, len(v), self.vars[nam].shape[0], self.vars[nam].shape[1]))
      self.vars[nam][self.ieq,:] = v      

   # return (bilinear) interpolated Flux for (x=R, y=z) coordinates
   def Rz2PFM(self, x, y):
      xx=self.R; yy=self.z
      zz=self.PFM[self.ieq,:,:].T
      x = np.interp(x,xx,np.arange(len(xx)))
      y = np.interp(y,yy,np.arange(len(yy)))
      
      x0 = np.floor(x).astype(int);x1 = x0 + 1
      y0 = np.floor(y).astype(int);y1 = y0 + 1
      
      x0 = np.clip(x0, 0, zz.shape[1]-1);x1 = np.clip(x1, 0, zz.shape[1]-1);
      y0 = np.clip(y0, 0, zz.shape[0]-1);y1 = np.clip(y1, 0, zz.shape[0]-1);

      wa = (x1-x) * (y1-y)
      wb = (x1-x) * (y-y0)
      wc = (x-x0) * (y1-y)
      wd = (x-x0) * (y-y0)
      
      return wa*zz[ y0, x0 ] + wb*zz[ y1, x0 ] + wc*zz[ y0, x1 ] + wd*zz[ y1, x1 ]   


   # "standardized" get functions for eqrzfd.from_shotfile()

   def num_equilibria(self):
      '''
        returns the number of equilibria stored in the CLISTE eqdsk file
      '''
      return(self.n_equil)

   def read_eqtimebase(self):
      '''
         returns an array with time points at which equilibria are available
      ''' 
      try:
         return self.vars['time']
      except:
         return None

   def select_eqtimes(self, eqtimes, eqtimebase=None):
      '''
        set time points at which equilibria are to be returned

        :eqtimes:  array of requested time points
      '''
      if eqtimebase is None:
         eqtimebase = self.read_eqtimebase()
      ix = [np.argmin(np.abs(eqtimebase-t)) for t in eqtimes]
      self.eqtindx = np.unique(ix)


   def read_fluxmatrix(self):
      '''
         read and return:
         PsiRZ (time,R,Z)  flux matrix
         R                 radial base
         Z                 elevation base
      '''
      if self.eqtindx is not None:
         return self.PFM[self.eqtindx,:,:], self.R, self.z
      return self.PFM, self.R, self.z  # flux in Vs/rad, COCO 17->7


   def read_fluxfunctions(self):
      '''
      return dictionary of flux functions:
        Psi, Qpsi, Pres, pprime, Fpol, FFprime, Vol, Volprime, Area, Areaprime
      '''
      if self.eqtindx is None:
         p = { "Psi": self.vars['PFL'] }
         p["Qpsi"] = self.vars['Qpsi']
         p["Pres"] = self.vars['Pres']
         p["pprime"] = self.vars['Presp']
         p["Fpol"] = self.vars['Jpol'] * 2.0e-7
         p["FFprime"] = p['Fpol']*self.vars['Jpolp']*2.0e-7
         p["Vol"] = self.vars['Vol'] 
         p["Volprime"] = self.vars['Vop'] 
         p["Area"] = self.vars['Area'] 
         p["Areaprime"] = self.vars['Areap']
      else:
         p = { "Psi": self.vars['PFL'][self.eqtindx,:] }
         p["Qpsi"] = self.vars['Qpsi'][self.eqtindx,:]
         p["Pres"] = self.vars['Pres'][self.eqtindx,:]
         p["pprime"] = self.vars['Presp'][self.eqtindx,:]
         p["Fpol"] = self.vars['Jpol'][self.eqtindx,:] * 2.0e-7
         p["FFprime"] = p['Fpol']*self.vars['Jpolp'][self.eqtindx,:]*2.0e-7
         p["Vol"] = self.vars['Vol'][self.eqtindx,:] 
         p["Volprime"] = self.vars['Vop'][self.eqtindx,:] 
         p["Area"] = self.vars['Area'][self.eqtindx,:] 
         p["Areaprime"] = self.vars['Areap'][self.eqtindx,:]
      return p

   
   def read_eqscalars(self):
      '''
        return dictionary with time traces of:
          Psimag, Psibdry, Rmaxis, zmaxis, Iplasma
      '''      
      if self.eqtindx is None:
        ti = np.arange(self.n_equil)
      else:
        ti = self.eqtindx

      self.read_special_points()
        
      s = {"Psimag": self.Psisp[:,0] }
      s['Psibdry'] = self.Psisp[:,1]
      s['Rmaxis'] = self.Rsp[:,0]
      s['zmaxis'] = self.zsp[:,0]
      s['Iplasma'] = self.vars['Ip'][ti]
      s['ikCat'] = self.vars['ikCAT'][ti]
   
      return s

   
   def read_special_points(self):
      '''
        special points (centre, limiter, X-points)
        returns up to 4 special points for each equilibrium
        [:,0]  magnetic axis
        [:,1]  limiter flux (if plasma is limited)
        [:,1 or 2]  active X-point
        [:,2 or 3]  additional X-point
      '''
      if self.eqtindx is None:
        ti = np.arange(self.n_equil)
      else:
        ti = self.eqtindx

      ikCat = self.vars['ikCAT'][ti]
      category = np.array( [2 if c in [3,4] else (1 if c in [1,2] else 0) for c in ikCat] )

      # allocate result arrays
      nmax = 4
      Nsp = np.ones(len(ti), dtype=np.int32)  # should have mag axis
      Psi = np.zeros((len(ti), nmax))
      Rsp = np.zeros((len(ti), nmax))
      zsp = np.zeros((len(ti), nmax))

      # magnetic axis
      Psi[:,0] = self.vars['Faxis'][ti] 
      Rsp[:,0] = self.vars['Rmag'][ti]
      zsp[:,0] = self.vars['Zmag'][ti]

      # boundary flux from last (outermost) main plasma PFL entry
      #Also replace axis flux with max flux per time point
      Fbdry = np.zeros(len(ti))
      for k, i in enumerate(ti):
         self.vars['Faxis'][k] = np.max(self.vars['PFL'][i,:])
         Psi[k,0] = self.vars['Faxis'][k]
         ix0 = 0
         Fxpu = self.vars['Fxpu'][i] 
         Fxpo = self.vars['Fxpo'][i] 
         Rxpu = self.vars['Rxpu'][i]
         Rxpo = self.vars['Rxpo'][i]
         Zxpu = self.vars['Zxpu'][i]
         Zxpo = self.vars['Zxpo'][i]
         if category[i] == 1:    # limiter plasma
            ibdry = (self.vars['LPFp'])[i] - 1   # index to boundary: LPF counts from 1
            Psi[k, 1] = (self.vars['PFL'])[i, ibdry] 
            ix0 = 1              # X-pts start after that
         if ikCat[i] == 4:    # LSN
            ixpu = ix0 + 1       # primary X-pt
            ixpo = ix0 + 2       # secondary (upper) X-pt
         elif ikCat[i] == 3:  # USN
            ixpo = ix0 + 1       # primary X-pt
            ixpu = ix0 + 2       # secondary X-pt (lower or SF-type)
         else:         # limiter plasma
            # if no ADC or one upper and one lower X-pt:
            if (self.vars['swiX'][i]==0) or (Zxpo*Zxpu<0): 
               # then flux order decides which is primary X-point
               if np.abs(Fxpo-Psi[k,0]) < np.abs(Fxpu-Psi[k,0]):
                  ixpo = ix0 + 1
                  ixpu = ix0 + 2
               else:
                  ixpu = ix0 + 1
                  ixpo = ix0 + 2
            else:       # CLISTE assigned a swiX number, so take over CLISTE's X-pt classification
               ixpo = ix0 + 1       # primary X-pt
               ixpu = ix0 + 2       # secondary X-pt (lower or SF-type)
            
         Psi[k, ixpu] = Fxpu
         Rsp[k, ixpu] = Rxpu
         zsp[k, ixpu] = Zxpu
         Psi[k, ixpo] = Fxpo
         Rsp[k, ixpo] = Rxpo
         zsp[k, ixpo] = Zxpo
         Nsp[k] = ix0 + 3    # 1 axis + (0 or 1) limiter + 2 Xpts

         self.Psisp = Psi
         self.Rsp = Rsp
         self.zsp = zsp
         self.nsp = Nsp
         self.category = category
         
      return category, Nsp, Psi, Rsp, zsp

   
   def read_PFcurrents(self):
      '''
        read PF currents (CLE = equilibrium currents)
      '''
      if self.eqtindx is None:
        ti = np.arange(self.n_equil)
      else:
        ti = self.eqtindx

      # find names of currents from their Rc, Zc coordinates (sigh!)
      # this is done only once, for the first equilibrium
      ieq = ti[0]
      Icoils = {}
      for k, Rc1 in enumerate(self.Rc[ieq,:]):
         wh = (self.Rc[ieq,k] >= 0.95*Rcn) & (self.Rc[ieq,k] <= 1.05*Rcn) \
            & (self.Zc[ieq,k] >= Zcn_min)  & (self.Zc[ieq,k] <= Zcn_max)
         if np.any(wh):
            cname = NamN[wh][0]
         else:
            print("sfcliste.py: Coil %d not found in coil list - cannot return PF currents" % k)
         Icoils[cname] = self.Ice[:,k] / Nn[wh]
      return Icoils      


   def read_boundary(self):
      '''
           return (R,z) coordinates of plasma boundary
           no plasma boundary in CLISTE eqdsk
      '''
      return None, None


   def read_limiter(self):
      '''
        return (R,z) coordinates of (closed) limiter contour
      '''
      fn = 'augiii_vessel.asc'
      if self.shot > 41570:
        fn = 'augdiviio_vessel.asc'
      elif self.shot==35840 and (self.experiment=='TAL' or self.experiment=='tal'):
        print("TAL's test equilibrium for divIIo")
        fn = 'augdiviio_vessel.asc'  
      Rlim, zlim = self.read_aug_pfc(module_path+'/../aug/'+fn)
      return Rlim, zlim


   def read_aug_pfc(self, fn):
     """
     read ASDEX Upgrade pfc coordinates from an ascii file
     """

     try:
       f = open(fn, 'r')
     except:
       print ("Cannot open file %s" % fn)
       return None, None
     Rv = []
     zv = []
     for line in f:
       columns = line.split()
       if len(columns)>0:
         Rv.append(float(columns[0]))
         zv.append(float(columns[1]))
     f.close()
     return Rv, zv


     
   

# ---------------------------- test main program --------------------------------------
   
if __name__ == '__main__':
   import sys
   c = cliste_eqdsk (sys.argv[1])
   #c.time = 1 + np.arange(c.n_equil)   # time = equilibrium ordinal number

   # vessel coordinates
   Rv,Zv = c.read_aug_pfc('../aug/augdiviio_vessel.asc')
   
   # for ieq in range(c.n_equil):
   for ieq in [0,1,2] :

      ikCat = c.vars['ikCAT'][ieq]
      swiX = c.vars['swiX'][ieq]
      
      cat,Nsp,Psi,Rsp,zsp = c.read_special_points()
      Faxis = Psi[ieq,0]
      Fbdry = Psi[ieq,1]
      if cat[ieq] == 1:
         print("No=%d  ikCAT=%d  swiX=%d  Faxis = %f, LIMITER: %f, Fxp1=%f, Fxp2=%f"
               % (ieq+1, ikCat, swiX, Psi[ieq,0], Psi[ieq,1], Psi[ieq,2], Psi[ieq,3]))
         Fxp1 = Psi[ieq,2]
         Fxp2 = Psi[ieq,3]
      else:
         print("No=%d  ikCAT=%d  swiX=%d  Faxis = %f, Fxp1=%f, Fxp2=%f"
               % (ieq+1, ikCat, swiX, Psi[ieq,0], Psi[ieq,1], Psi[ieq,2]))
         Fxp1 = Psi[ieq,1]
         Fxp2 = Psi[ieq,2]

#      if (ikCat!=1):
#         continue

      PFM, Rg, Zg = c.read_fluxmatrix()
      Ncont = [30,21,21]
      
      fig,ax=plt.subplots()

      for ic in range(Nsp[ieq]-1):
         ax.contour(Rg, Zg, PFM[ieq,:,:].T, np.sort(np.linspace(Psi[ieq,ic], Psi[ieq,ic+1], Ncont[ic])))
         if ic>0:
            ax.contour(Rg, Zg, PFM[ieq,:,:].T, [Psi[ieq,ic]], colors='black')   # limiter/separatrix contour
         if ic==0:  
            ax.scatter(Rsp[ieq,0], zsp[ieq,0], color="red")    # axis 
         if (ic==1) & (Rsp[ieq,1]>0.1):  
            ax.scatter(Rsp[ieq,1], zsp[ieq,1], color="red")    # active boundary point (if given)
         if Nsp[ieq] > 2:
            ax.scatter(Rsp[ieq,2],  zsp[ieq,2], color="blue")   # secondary bdry point
            if Nsp[ieq] > 3:
               ax.scatter(Rsp[ieq,3:],  zsp[ieq,3:], color="magenta")   # tertiary bdry point(s)
            
      ax.plot(Rv, Zv, color='red')  # overplot vessel contour

      Zxpo = c.vars['Zxpo'][ieq]
      Zxpu = c.vars['Zxpu'][ieq]
      ax.set_title("No=%d  ikCAT=%d  swiX=%d  Zxpo=%5.3f Zxpu=%5.3f" % (ieq+1, ikCat, swiX, Zxpo, Zxpu))
      ax.set_aspect('equal')
      plt.show()


   if True:
      Ic = c.read_PFcurrents()
      
