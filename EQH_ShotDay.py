import aug_sfutils as sf
import numpy as np
import sys,time
import subprocess as sp

def CheckSF(shot,diag):
    p = sp.Popen(["shotls",diag,"%d"%shot],
                 stdout=sp.PIPE,stderr=sp.PIPE)
    out = p.stdout.readlines()
    if out:
        status = True
    else:
        status = False
    return status

jou = sf.JOURNAL()
if len(sys.argv) > 1:
    Shot = int(sys.argv[1])
else:
    Shot = int(jou.getLastShot())


while True:
    jou = sf.JOURNAL() #refresh journal DB
    LastShot = int(jou.getLastShot())
    print(LastShot,Shot)
    if LastShot >= Shot: #check that Shot has actually been performed
        lastEQH = CheckSF(Shot,'EQH')
        if lastEQH:
            print('EQH written for last shot')
        else:
            print("EQH not written for last shot, trying now")
            print(Shot)
            count = 0
            while count < 3:
                lastFPG = CheckSF(Shot,'FPG')
                if lastFPG:
                    sp.run(["python","EQH_workflow.py","%d"%Shot,"-tdist","0.001","-diaW","EQH"])
                    count = 3
                else:
                    print("FPG not written, waiting 5 minutes")
                    count += 1
                    time.sleep(5*60) #wait 5 minutes
                print("EQH either written or timed out, iterating shot")
        Shot += 1
    else:
        print("Waiting")
        time.sleep(60)
    
