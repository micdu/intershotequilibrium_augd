import sys,os
import time
import subprocess as sp
import numpy as np
from datetime import datetime

import matplotlib.pyplot as plt
import aug_sfutils as sf

from makeMBI import makeMBI
from writeMAP import writeMAP
from makeFPG import makeFPG

timeout_after_shotnumeber_distribution = 1 # s
timeout_check = 120 #s
timeout_eqi = 30 * 60 #s

def CheckNewShot(previousShot):
    lastShot = sf.getlastshot()
    print(previousShot,lastShot)
    if lastShot > 90000:
        print('Last shot set to 90000')
        lastShot = 90000
    print(previousShot,lastShot)
    if lastShot != previousShot:
        print('new shot number')
        new = previousShot +1
    else:
        new = None
        
    return new

def CheckSF(shot,diag):
    p = sp.Popen(["shotls",diag,"%d"%shot],
                 stdout=sp.PIPE,stderr=sp.PIPE)
    out = p.stdout.readlines()
    if out:
        status = True
    else:
        status = False
    return status


class ShotControl:
    def __init__(self,shot):
        self.shot = shot        
        self.prereqs = {'MAY':False,'MAP':False,'MBI':False,'FPG':False}
        
        self.ReadyEQI = False
        self.DoneEQI = False
        self.DoneEQH = False

        self.RunEQH = False
        self.WhereEQH = None
        
        self.CheckPrereq() #First call to check prereqs
        self.CheckPrereq() #If all prereqs then EQI = ready
        self.LowIp = self.CheckIp()
        
        self.DoneEQI = CheckSF(self.shot,'EQI')
        self.DoneEQH = CheckSF(self.shot,'EQH')
        if self.LowIp:
            self.DoneEQI = True
            self.DoneEQH = True

        self.Error = False
    

    def CheckPrereq(self):
        if self.prereqs['MAY'] and\
           self.prereqs['MAP'] and\
           self.prereqs['MBI'] and\
           self.prereqs['FPG']:
            self.ReadyEQI = True
        else:
            for diag,status in self.prereqs.items():
                status = CheckSF(self.shot,diag)
                self.prereqs[diag] = status
            if self.prereqs['MAY']:
                if not self.prereqs['MBI']:
                    makeMBI(self.shot)
                if not self.prereqs['MAP']:
                    writeMAP(self.shot)
                if not self.prereqs['FPG']:
                    if self.prereqs['MAP'] and\
                       self.prereqs['MBI']:
                        makeFPG(self.shot,FPG_experiment='AUGD')
                    else:
                        print('Cannot yet write FPG')
            else:
                print('MAY not yet written, cannot progress')
                    

    def CheckIp(self):
        status = False
        if self.prereqs['FPG']:
            sfo = sf.SFREAD('FPC',self.shot)
            ip = sfo.getobject('IpiFP')
            tip = sfo.gettimebase('IpiFP')
            tic = np.trapz(ip,x=tip)/1e3
            print(tic)
            if tic < 100.:
                status = True
                print('Ip too low/short')
        return status

                
MachineFree = {'sxaug33':True,'sxaug37':True}
firstShot = int(sys.argv[1])

newshot = CheckNewShot(firstShot)
#AllShots = [ShotControl(firstShot)]
AllShots = [ShotControl(firstShot)]
#for sn in newshots:
AllShots.append(ShotControl(newshot))
'''
if len(newshots) > 0:
    lastShot = newshots[-1]
else:
    lastShot = firstShot
'''
lastShot = AllShots[-1].shot

while True:
    newshot = CheckNewShot(lastShot)
    print(newshot)
    #for sn in newshots:
    if newshot:
        AllShots.append(ShotControl(newshot))    
    lastShot = AllShots[-1].shot
    for shot in AllShots:
        print(shot.shot)
    '''
    try:
        if proc33.poll() is None:
            print('sxaug33 is running')
        else:
            print(proc33.poll())
            #os.killpg(os.getpgid(proc33.pid), signal.SIGTERM)
            MachineFree['sxaug33'] = True
    except:
        print('sxaug33 not yet called')
    try:
        if proc37.poll() is None:
            print('sxaug37 is running')
        else:
            print(proc37.poll())
            #os.killpg(os.getpgid(proc37.pid), signal.SIGTERM)
            MachineFree['sxaug37'] = True
    except:
        print('sxaug37 not yet called')
    '''
        
    for shot in AllShots:
        #print('Shot number:')
        #print(shot.shot)

        if not shot.DoneEQI:
            shot.DoneEQI = CheckSF(shot.shot,'EQI')
            shot.CheckPrereq()
            if not shot.DoneEQI and shot.ReadyEQI:
                shot.LowIp = shot.CheckIp() #saves LowIp to fixed variable now
                if shot.LowIp:
                    shot.DoneEQI = True
                    shot.DoneEQH = True
                    print('Not enough current/time for EQI in shot %d'%shot.shot)
                else:
                    print('Running EQI')
                    tun = sp.Popen(['python','EQH_workflow.py',"%d"%shot.shot])
                    #output, error = tun.communicate("/u/micdu/equil/cliste/EQI_gen/makeEQ %d"%shot.shot)
                    #print(output)
                    #print(error)
                    #print(tun.returncode)
            else:
                print('Shot %d not ready for EQI'%shot.shot)
        
        '''
        if not shot.DoneEQH and shot.DoneEQI: #don't check for EQH if no EQI
            print(shot.shot)
            
            shot.DoneEQH = CheckSF(shot.shot,'EQH')
            shot.CheckPrereq()
            if not shot.DoneEQH and shot.ReadyEQI:
                if shot.LowIp:
                    shot.DoneEQH = True
                    #print('Not enough current/time for EQH in shot %d'%shot.shot)
                else:
                    if MachineFree['sxaug33']:
                        proc33 = sp.Popen(["/u/micdu/equil/cliste/EQI_gen/makeEQH_new","%d"%shot.shot,"--host","sxaug33"])
                        MachineFree['sxaug33'] = False
                        shot.DoneEQH = True
                    elif MachineFree['sxaug37']:
                        proc37 = sp.Popen(["/u/micdu/equil/cliste/EQI_gen/makeEQH_new","%d"%shot.shot,"--host","sxaug37"])
                        MachineFree['sxaug37'] = False
                        shot.DoneEQH = True
                    elif MachineFree['sxaug35']:
                        proc35 = sp.Popen(["/u/micdu/equil/cliste/EQI_gen/makeEQH_new","%d"%shot.shot,"--host","sxaug35"])
                        MachineFree['sxaug35'] = False
                        shot.DoneEQH = True
                    elif MachineFree['sxaug31']:
                        proc31 = sp.Popen(["/u/micdu/equil/cliste/EQI_gen/makeEQH_new","%d"%shot.shot,"--host","sxaug31"])
                        MachineFree['sxaug31'] = False
                        shot.DoneEQH = True
                    else:
                        print('All machines running something')
        '''
        if shot.Error:
            print('Shot %d has an error, check it out!'%shot.shot)
                    

    time.sleep(timeout_check)
