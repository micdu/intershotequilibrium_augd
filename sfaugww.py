'''
   sfaugww.py  - write AUG shot files
                 using git's augsf_utils

   Initially written by wls
   Adapted for CLISTE SF writing my micdu, Feb 2024
'''

import inspect, os
import tempfile
import numpy as np
import shutil     # for copyfile()
import ctypes as ct
import getpass    # for getuser()
from utils.make_fpsfh import make_fpsfh

# shot file header is in the directory of this module
# import inspect, os
# sfhdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

import aug_sfutils
module_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import pdb

SSQnam = np.array(['Rsquad','Zsquad','Rgeo','Zgeo','Rxpu','Zxpu','ahor','k','Rin','Raus','Zunt','Rzunt','Zoben','Rzoben','Rmag','Zmag','Vol','IVSF','Slunt','Srunt','fXP1fIL','fXP1fAL','XPfdif','lenH-1','lenH-2','lenH-0','Rxpo','Zxpo','Slobn','Srobn','bpli2','betpol','li','Wmhd','Circumf','bver','q95/q0','Finstab','Fstab','Rax-Rgeo','fax-bnd','fbnd-ref','q0','q95','fbnd-f12','fbnd-f17','fbnd-f22','fbnd-f25','q25','q50','q75','Zgeri','Zgera','Zskewi','Zskewa','Rroofi','Rroofa','lenH-4','delRoben','delRuntn','GapAbot','GapAtop','GapAmin','lenH-5','lenV-1','koben','kunten','lenV-2','lenV-3','dRXP','ray___15','ray___30','ray___45','ray___60','ray___75','ray___90','ray__105','ray__120','ray__135','ray__150','ray__165','ray__180','ray__195','ray__210','ray__225','ray__240','ray__255','ray__270','ray__285','ray__300','ray__315','ray__330','ray__345','ray__000','Ip'])

FPGnam = np.array(['Rsquad','Zsquad','Rgeo','Zgeo','Rxpu','Zxpu','ahor','k','Rin','Raus','Zunt','Rzunt','Zoben','Rzoben','Rmag','Zmag','IVSF','Slunt','Srunt','fXP1fIL','fXP1fAL','XPfdif','lenH-0','lenH-1','lenH-2','Rxpo','Zxpo','Slobn','Srobn','bpli2','betpol','li','Wmhd','Circumf','bver','q95/q0','Finstab','Fstab','Rax-Rgeo','fax-bnd','fbnd-ref','q0','q95','fbnd-f12','fbnd-f17','fbnd-f22','fbnd-f25','q25','q50','q75','lenH-4','delRoben','delRuntn','GapAbot','GapAtop','GapAmin','lenH-5','lenV-1','koben','kunten','lenV-2','lenV-3','dRXP','Zgeri','Zgera','Zskewa','Zskewi','Runi2b','Zuni2b','Suni2b','Runa2b','Zuna2b','Suna2b','Robi2','Zobi2','Sobi2','Roba2','Zoba2','Soba2'])

FPGrenames = {'kunten':'kuntn'}

RAYnam = np.array(['ray___15','ray___30','ray___45','ray___60','ray___75','ray___90','ray__105','ray__120','ray__135','ray__150','ray__165','ray__180','ray__195','ray__210','ray__225','ray__240','ray__255','ray__270','ray__285','ray__300','ray__315','ray__330','ray__345','ray__000'])

FPdiag = {'EQI': 'GQI', 'FPP': 'FPG', 'EQH': 'GQH',
          'EQB': 'GQB', 'EQK': 'GQK', 'EDB': 'GDB'}

# ============== write  an EQI-type shot file from eqrzfd instance ============

def write_augsf_eqrzfd(shot, diag, eq, description=None, \
                       experiment='self', verbose=False):
    '''
      write an eqrzfd instance (free boundary equilibrium on 2D rectanagular grid)
      to an AUG shotfile  (experiment:diag shot)

      eq: Equilibrium instance

    '''
    if experiment=='self':
        experiment = getpass.getuser()    
    # ................ create shot file header .................
    # shot file header file name, as dictated by libddww
    sfhname = '%s00000.sfh' % (diag)

    # copy from template into a unique directory we can write to
    # shutil.copyfile(module_path+'/'+'EQT00000.sfh', sfhname)

    td = tempfile.TemporaryDirectory()
    print("Temporary directory: ", td.name)
    shutil.copyfile(module_path+'/SFH/'+'EQT00000.sfh', td.name+'/'+sfhname)
    old_wd = os.getcwd()
    os.chdir(td.name)

    # ................. edit shot file header ..................
    sfh = aug_sfutils.sfh.SFH()
    err = sfh.Open(sfhname)
    if err!=0:
        print("sfh.Open()  returns", err)

    # rename diagnostic
    diagname = aug_sfutils.str_byt.to_byt(diag)
    c_diag = ct.c_char_p(diagname)
    err = aug_sfutils.sfh.libsfh.sfhrename(sfh.c_sfid, c_diag)
    aug_sfutils.sfh.libsfh.sfherror(err, 'sfhrename')

    # add description
    if description is not None:
        sfh.set_text(diag, description)

    # set signal lengths
    ntimes = len(eq.time)
    nR = len(eq.R)
    nz = len(eq.z)
    npsi = eq.PFL.shape[1]
    if eq.Psisp is not None:
        neqsp = eq.Psisp.shape[1]
    else:
        neqsp = 0
    
    nsp = max(5, neqsp, np.max(eq.nsp))  # space to allocate
    nssq = SSQnam.size  # several scalar quantities: for later
    ncl = eq.ncoils

    # find numbers of inner and outer flux labels
    nin = np.zeros(ntimes)
    nout = np.zeros(ntimes)
    npsi_eff = npsi
    for i in range(ntimes):
        Psin = (eq.PFL[i,:] - eq.Psimag[i]) / (eq.Psibdry[i] - eq.Psimag[i])
        nin = np.count_nonzero(Psin <= 1.0)
        nout = np.count_nonzero(Psin > 1.0)
        if nout>1:     # have open flux surfaces
            npsi1 = nin + 1 + nout
        else:          # closed flux surfaces only
            npsi1 = nin
        npsi_eff = max(npsi_eff, npsi1)

    # write signal dimensions
    sfh.Modtim("ixti", ntimes)
    sfh.Modtim("time", ntimes)
    sfh.Modsgr("Rcl", [eq.ncoils,1])
    sfh.Modsgr("Zcl", [eq.ncoils,1])
    sfh.Modsgr("Ri", [nR, ntimes])
    sfh.Modsgr("Zj", [nz, ntimes])
    sfh.Modsgr("PFM", [nR, nz, ntimes])
    sfh.Modsgr("Lpf", [1, ntimes])
    sfh.Modsgr("PFL", [npsi_eff, ntimes])
    sfh.Modsgr("TFLx", [npsi_eff, ntimes])
    sfh.Modsgr("Qpsi", [npsi_eff, ntimes])
    sfh.Modsgr("Jpol", [2*npsi_eff, ntimes])
    sfh.Modsgr("Pres", [2*npsi_eff, ntimes])
    sfh.Modsgr("Vol", [2*npsi_eff, ntimes])
    sfh.Modsgr("Area", [2*npsi_eff, ntimes])
    sfh.Modsgr("FFP", [npsi_eff, ntimes])
    sfh.Modsgr("Rinv", [npsi_eff, ntimes])
    sfh.Modsgr("R2inv", [npsi_eff, ntimes])
    sfh.Modsgr("Bave", [npsi_eff, ntimes])
    sfh.Modsgr("B2ave", [npsi_eff, ntimes])
    sfh.Modsgr("FTRA", [npsi_eff, ntimes])
    sfh.Modsgr("CLD", [ncl, ntimes])
    sfh.Modsgr("CLE", [ncl, ntimes])
    sfh.Modsgr("LPFx", [1, ntimes])
    sfh.Modsgr("PFxx", [nsp, ntimes])
    sfh.Modsgr("RPFx", [nsp, ntimes])
    sfh.Modsgr("zPFx", [nsp, ntimes])
    sfh.Modsgr("ikCAT", [1, ntimes])
    sfh.Modsgr("IpiPSI", [1, ntimes])
    sfh.Modsgr("SSQnam", [8, nssq])
    sfh.Modsgr("LSSQ", [1, ntimes])
    sfh.Modsgr("SSQ", [nssq, ntimes])
    
    
    
    print('finished editing shot file header')
    err = sfh.Close()
    if err!=0:
        print("sfh.Close()  returns", err)

    # copy  sfh for debugging
    shutil.copyfile(td.name+'/'+sfhname, old_wd+'/'+sfhname)
   # shutil.copyfile(sfhname, '/afs/ipp-garching.mpg.de/u/wls/w/weqtools/'+sfhname)

#    os.chdir(old_wd)  # back to where we were before

    # allocate space for flux functions
    Lpf = np.zeros((1, ntimes), dtype=int)
    PFL = np.zeros((npsi_eff, ntimes))
    TFLx = np.zeros((npsi_eff, ntimes))
    Qpsi = np.zeros((npsi_eff, ntimes))
    FFP = np.zeros((npsi_eff, ntimes))
    Jpol = np.zeros((2*npsi_eff, ntimes))
    Pres = np.zeros((2*npsi_eff, ntimes))
    Vol = np.zeros((2*npsi_eff, ntimes))
    Area = np.zeros((2*npsi_eff, ntimes))
    Rinv = np.zeros((npsi_eff,ntimes))
    R2inv = np.zeros((npsi_eff,ntimes))
    Bave = np.zeros((npsi_eff,ntimes))
    B2ave = np.zeros((npsi_eff,ntimes))
    FTRA = np.zeros((npsi_eff,ntimes))

    # allocate space for scalars
    IpiPSI = np.zeros((1, ntimes))
    ikCAT = np.zeros((1, ntimes), dtype=int)
    LPFx = np.zeros((1, ntimes), dtype=int)
    PFxx = np.zeros((nsp, ntimes))
    RPFx = np.zeros((nsp, ntimes))
    zPFx = np.zeros((nsp, ntimes))

    Rcl = np.zeros((eq.ncoils,1))
    Zcl = np.zeros((eq.ncoils,1))
        
    Rcl[:,0] = np.transpose(eq.Rc[0,:])
    Zcl[:,0] = np.transpose(eq.Zc[0,:])
    
    # several scalar quantities (SSQ)
    #SSQnam = np.array([ 'Iplasma', 'Rmag', 'Zmag'])

    #sort time array and fix everything below to increasing time
    sorting = np.argsort(eq.time)

    
    ikCAT[0,:] = eq.vars['ikCAT'][sorting]
    FPGdict = {}
    nssq = len(SSQnam)
    LSSQ = np.expand_dims(np.repeat(nssq, ntimes), axis=0)
    SSQ = np.zeros((nssq, ntimes))
    for i,nam in enumerate(SSQnam):
        try:
            SSQ[i,:] = eq.vars[nam][sorting]
        except:
            print('SSQ not found in equil:',nam)
    for nam in FPGnam:
        try:
            FPGdict[nam] = eq.vars[nam][sorting]
        except:
            print('FP variable not found',nam)
            FPGdict[nam] = time * 0.
    rays = []
    for nam in RAYnam:
        rays.append(eq.vars[nam][sorting])
    #FPGdict['rays'] = np.array(rays)
    
    FPGdict['ikCAT'] = ikCAT.squeeze()
    FPGdict['TIMEF'] = eq.time[sorting]
    
    FPGdict['rays'] = np.transpose(np.asarray(rays))
    #Rename Zskew variables
    FPGdict['Zskewi2b'] = FPGdict.pop('Zskewi')
    FPGdict['Zskewa2b'] = FPGdict.pop('Zskewa')
    
    wh_vol = int(np.where(SSQnam == 'Vol')[0])
    # pack flux functions, scalars into shot file format
    for i in range(ntimes):
        if nout>1:     # have open flux surfaces
            Lpf[0, i] = (nin-1) + 10000*(nout-1)
            ix = np.hstack((np.arange(nin-1,-1,-1), [0],  np.arange(nin-1,nin+nout-1,1)))
        else:          # closed flux surfaces only
            Lpf[0, i] = (nin-1)
            ix = np.arange(nin-1,-1,-1)  # reverse order: edge to core
        n = len(ix) ; n2e = 2*n ; n2o = n2e + 1
        PFL[0:n,i] = eq.PFL[sorting[i], ix]
        FFP[0:n,i] = eq.Jpol[sorting[i], ix] * eq.Jpolp[sorting[i],ix] * 4e-14
        Jpol[0:n2e:2, i] = eq.Jpol[sorting[i],ix] 
        Jpol[1:n2o:2, i] = eq.Jpolp[sorting[i],ix] 
        Pres[0:n2e:2, i] = eq.Pres[sorting[i],ix] 
        Pres[1:n2o:2, i] = eq.Presp[sorting[i],ix] 
        Qpsi[0:n,i] = eq.Qpsi[sorting[i], ix]
        Rinv[0:n,i] = eq.Rinv[sorting[i],ix]
        R2inv[0:n,i] = eq.R2inv[sorting[i],ix]
        Bave[0:n,i] = eq.Bave[sorting[i],ix]
        B2ave[0:n,i] = eq.B2ave[sorting[i],ix]
        FTRA[0:n,i] = eq.FTRA[sorting[i],ix]
        TFLx[0:n,i] = eq.TFL[sorting[i],ix]
        
        ffkeys = eq.fluxfunctions.keys()
        if "Vol" in ffkeys and "Volprime" in ffkeys:
            Vol[0:n2e:2, i] = (eq.fluxfunctions["Vol"])[sorting[i],ix] 
            Vol[1:n2o:2, i] = (eq.fluxfunctions["Volprime"])[sorting[i],ix]
            SSQ[wh_vol,i] = (eq.fluxfunctions["Vol"])[sorting[i],ix][0]
        if "Area" in ffkeys and "Areap" in ffkeys:
            Area[0:n2e:2, i] = (eq.fluxfunctions["Area"])[sorting[i],ix] 
            Area[1:n2o:2, i] = (eq.fluxfunctions["Areaprime"])[sorting[i],ix]            
        FPGdict['Vol'] = SSQ[wh_vol,sorting]

        
        
        '''
        if eq.Rsp is not None and eq.zsp is not None:
            if eq.category[i]>0:   # know if limited or diverted
                if eq.category[i]==1:     #  limited
                    if eq.Rsp[i, 1] < eq.Rmaxis[i]:
                        ikCAT[0, i] = 1   # inner limiter
                    else:
                        ikCAT[0, i] = 2   # outer limiter
                if eq.category[i]==2:     #  diverted
                    if eq.zsp[i, 1] > eq.zsp[i,0]:
                        ikCAT[0, i] = 3   # USN
                    else:
                        ikCAT[0, i] = 4   # LSN
        '''
        
    for key,val in FPGrenames.items():
        FPGdict[val] = FPGdict.pop(key)

    # special points
    #first, sort special points:
    eq.Psisp = eq.Psisp[sorting,:]
    eq.Rsp = eq.Rsp[sorting,:]
    eq.zsp = eq.zsp[sorting,:]
    
    eq.vars['Fx2'] = eq.vars['Fx2'][sorting]
    eq.eqres.vars['Rx2'] = eq.eqres.vars['Rx2'][sorting]
    eq.eqres.vars['Zx2'] = eq.eqres.vars['Zx2'][sorting]

    eq.vars['Fil'] = eq.vars['Fil'][sorting]
    eq.eqres.vars['Ril'] = eq.eqres.vars['Ril'][sorting]
    eq.eqres.vars['Zil'] = eq.eqres.vars['Zil'][sorting]
    
    eq.vars['Fol'] = eq.vars['Fol'][sorting]
    eq.eqres.vars['Rol'] = eq.eqres.vars['Rol'][sorting]
    eq.eqres.vars['Zol'] = eq.eqres.vars['Zol'][sorting]
    
    if eq.Psisp is not None:   # have a full list
        for i in range(ntimes):
            PFxx[0, i] = eq.Psisp[i, 0] #* (2*np.pi)   # magnetic axis
            RPFx[0, i] = eq.Rsp[i, 0]
            zPFx[0, i] = eq.zsp[i, 0]
            LPFx[0, i] = nsp-1 #for reasons unknown, LPFx = nspec - 1

            if ikCAT[0,i] == 1 or ikCAT[0,i] == 2:  # limiter
                PFxx[2, i] = eq.Psisp[i, 1] #* (2*np.pi)  # active limiter
                RPFx[2, i] = eq.Rsp[i, 1]
                zPFx[2, i] = eq.zsp[i, 1]
                PFxx[1, i] = -7777.0    # SF code for no active special point
                PFxx[3, i] = -7777.0
                PFxx[4, i] = -7777.0
            if ikCAT[0,i] == 3 or ikCAT[0,i] ==4:   # divertor
                PFxx[1, i] = eq.Psisp[i, 1] #* (2*np.pi) # primary X-point
                RPFx[1, i] = eq.Rsp[i, 1]
                zPFx[1, i] = eq.zsp[i, 1]
                
                PFxx[3, i] = eq.vars['Fx2'][i] #* (2*np.pi) # seconday X-point
                RPFx[3, i] = eq.eqres.vars['Rx2'][i]
                zPFx[3, i] = eq.eqres.vars['Zx2'][i]

                if eq.vars['Fil'][i] > eq.vars['Fol'][i]:
                    PFxx[2, i] = eq.vars['Fil'][i]
                    RPFx[2, i] = eq.eqres.vars['Ril'][i]
                    zPFx[2, i] = eq.eqres.vars['Zil'][i]
                    PFxx[4, i] = eq.vars['Fol'][i]
                    RPFx[4, i] = eq.eqres.vars['Rol'][i]
                    zPFx[4, i] = eq.eqres.vars['Zol'][i]
                else:
                    PFxx[4, i] = eq.vars['Fil'][i]
                    RPFx[4, i] = eq.eqres.vars['Ril'][i]
                    zPFx[4, i] = eq.eqres.vars['Zil'][i]
                    PFxx[2, i] = eq.vars['Fol'][i]
                    RPFx[2, i] = eq.eqres.vars['Rol'][i]
                    zPFx[2, i] = eq.eqres.vars['Zol'][i]
            else:                      # improvise
                LPFx[0, :] = np.repeat(2, ntimes)
                PFxx[0, :] = eq.Psimag[sorting] #* (2*np.pi)
                RPFx[0, :] = eq.Rmaxis[sorting]
                zPFx[0, :] = eq.zmaxis[sorting]
                PFxx[1, :] = eq.Psibdry #* (2*np.pi)

    #print(np.argsort(eq.time))
    #print(np.diff(FPGdict['TIMEF']))
    # build dictionary and write it to shot file
    s = {'PARMV': {'M': nR-1, 'N': nz-1, 'NTIME': ntimes}, \
         'time': eq.time[sorting], 'ixti':  np.arange(ntimes), \
         'Rcl': Rcl, 'Zcl': Zcl, \
         'Ri':  np.repeat(np.expand_dims(eq.R, axis=1), ntimes, axis=1), \
         'Zj':  np.repeat(np.expand_dims(eq.z, axis=1), ntimes, axis=1), \
         'PFM': np.transpose(eq.PFM, axes=[1,2,0])[:,:,sorting], \
         'Lpf': Lpf, 'PFL': PFL, 'TFLx': TFLx, 'FFP': FFP, 'Jpol': Jpol, 'Pres': Pres, \
         'Qpsi': Qpsi, 'Vol': Vol, 'Area': Area, \
         'Rinv': Rinv, 'R2inv': R2inv, 'Bave': Bave,\
         'B2ave': B2ave, 'FTRA':FTRA,\
         'LPFx': LPFx, 'PFxx': PFxx, 'RPFx': RPFx, 'zPFx': zPFx, \
         'ikCAT': ikCAT, 'IpiPSI': IpiPSI, \
         'SSQnam': SSQnam, 'LSSQ': LSSQ, 'SSQ': SSQ, \
         'CLD': eq.Icd, 'CLE': eq.Ice 
         }

    aug_sfutils.ww.write_sf(shot, s, td.name, diag, exp=experiment)
    #write FP-equivalent
    make_fpsfh(list(FPGdict.keys()),FPdiag[diag],'Real equil',eq.time.size)
    aug_sfutils.ww.write_sf(shot,FPGdict,old_wd+'/SFH/',FPdiag[diag], exp=experiment)
    os.chdir(old_wd)  # back to where we were before

    return old_wd

    
    
    
# ============================= test program ==================================

if __name__ == '__main__':

    import sys
    # ---------- test a shotfile with signal group dictionary ------------

    from cliste_eqdsk import cliste_eqdsk
    shot = sys.argv[1]
    experiment = 'micdu'
    diag = 'EQI'
    description = 'created by weqpost'
    dir = sys.argv[2]
    eq = cliste_eqdsk(dir+'/eqdsk',fn_eqres=dir+'/result')
    
    write_augsf_eqrzfd(int(shot), diag, eq, description=description, 
                       experiment=experiment, verbose=True)
    
    
