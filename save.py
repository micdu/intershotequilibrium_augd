import sys
rootdir = sys.argv[2]
from sfaugww import write_augsf_eqrzfd as eqww
from cliste_eqdsk import cliste_eqdsk

shot = int(sys.argv[1])
eq = cliste_eqdsk(rootdir+'/eqdsk',fn_eqres=rootdir+'/result')
eqww(shot,'EQH',eq,experiment='AUGD')
