import numpy as np
import sys,time
import subprocess as sp

import aug_sfutils as dd
from utils.warning import warning

def CheckSF(shot,diag):
    p = sp.Popen(["shotls",diag,"%d"%shot],
                 stdout=sp.PIPE,stderr=sp.PIPE)
    out = p.stdout.readlines()
    if out:
        status = True
    else:
        status = False
    return status

jou = dd.JOURNAL()
if len(sys.argv) > 1:
    shot1 = int(sys.argv[1])
else:
    shot1 = int(jou.getLastShot())

wait = 60*3.

while True:
    MBImissing = []
    MAPmissing = []
    FPGmissing = []
    EQImissing = []
    EQHmissing = []

    jou = dd.JOURNAL() #refresh journal DB
    shot2 = int(jou.getLastShot())

    for shot in range(shot1,shot2+1):
        if (jou.isUseful(shot)) and (jou.getShotEntry(shot,'Typ') == 'plasma') :
            if not CheckSF(shot,'MBI'):
                MBImissing.append(str(shot))
            if not CheckSF(shot,'MAP'):
                MAPmissing.append(str(shot))
            if not CheckSF(shot,'FPG'):
                FPGmissing.append(str(shot))
            if not CheckSF(shot,'EQI'):
                EQImissing.append(str(shot))
            if not CheckSF(shot,'EQH'):
                EQHmissing.append(str(shot))


    count_missing = len(MBImissing)+len(MAPmissing)+len(FPGmissing)+len(EQImissing)

    if count_missing > 0:
        MBIstring = ','.join(MBImissing)
        MAPstring = ','.join(MAPmissing)
        FPGstring = ','.join(FPGmissing)
        EQIstring = ','.join(EQImissing)
        text = 'Missing shots per diagnostic:\nMBI: '+MBIstring+'\nMAP: '+MAPstring+'\nFPG: '+FPGstring+'\nEQI: '+EQIstring
        warning(text)

    if len(MBImissing) > 0:
        print('All useful shots missing MBI:')
        print(MBImissing)
    if len(MAPmissing) > 0:
        print('All useful shots missing MAP:')
        print(MAPmissing)
    if len(FPGmissing) > 0:
        print('All useful shots missing FPG:')
        print(FPGmissing)

    if len(EQImissing) > 0:
        print('All useful shots missing EQI:')
        print(EQImissing)
    if len(EQHmissing) > 0:
        print('All useful shots missing EQH:')
        print(EQHmissing)

    print('Waiting at ',time.ctime())
    time.sleep(wait)
