#! /usr/bin/perl -w
use strict;

use Getopt::Long;
use Socket;
use IO::Handle;     # thousands of lines just for autoflush :-(
use Cwd qw(abs_path);


my $bin_cliste  = '/shares/departments/AUG/users/micdu/equil/cliste/cliste_aug/lib_citrix/cliste.out';
my $infile    = '/shares/departments/AUG/users/micdu/equil/cliste/data/AUGcliste.in.eqh';
my $flagsfile = '/shares/departments/AUG/users/micdu/equil/cliste/data/DivIII.flags.eqh';
my $mdfile    = '/shares/departments/AUG/users/micdu/equil/cliste/sun/MDfiles/AUG_MachineDescriptionDO_Oct2024.xml';

my $njobs     = 9;
my $tmpdir = '/tmp/';
my $allowCore = 0;
my $grepcens  = 0;
my $ifxtr = 0;

my %action;

my $commandLine = $0 . ' ' . join(' ',  @ARGV);
print STDERR $commandLine, "\n";

GetOptions(
    'run!'		=> \$action{run},
    
    'bin_cliste=s'	=> \$bin_cliste,
    'infile=s'		=> \$infile,
    'flagsfile=s'	=> \$flagsfile,
    'mdfile=s'		=> \$mdfile,

    'njobs=i'		=> \$njobs,
    'tmpdir=s'		=> \$tmpdir,
    'core!'		=> \$allowCore,
    'grepcens!'		=> \$grepcens,

    'help'              => \&printUsage,
)
or printUsage();

my @executablesToCheck = ($bin_cliste);

foreach my $file (@executablesToCheck) {
    my $file_x = abs_path($file);
    if (! -x $file_x) {
	die "$file is not executable";
    }
    $file = $file_x;
}

foreach my $file ($infile, $flagsfile, $mdfile) {
    my $file_x = abs_path($file);
    if (! -r $file_x) {
	die "$file is not readable";
    }
    $file = $file_x;
}

#=============================================================================
# Initialize
chdir($tmpdir) or die "Cannot chdir $tmpdir: $!";

open STDOUT, '>', 'stdout';
open STDERR, '>', 'stderr';

print STDERR $commandLine, "\n\n";
$ENV{TERM} = 'xterm';
print STDERR join("\n", map {"$_=$ENV{$_}"} sort(keys(%ENV))), "\n\n";
system('/bin/tty');

#if (! $allowCore) {

#    use BSD::Resource;
#    setrlimit( BSD::Resource::RLIMIT_CORE(),  0, RLIM_INFINITY );
#    system('/usr/bin/ulimit -c');
#}

my $slashOld = $/;
undef $/;
open FLAGS, '<', $flagsfile
    or die "Cannot read $flagsfile: $!";
my $flags = <FLAGS>;
close FLAGS;

# Try to get the cliste revision
my $clistepath = $bin_cliste;
$clistepath =~ s{/[^/]*$}{};

my $rev = 0;
if ($rev==0 && $bin_cliste =~ m{(\d{3,})}) {
    $rev = $1;
}

my $startline = 'v-'x80 . "\n";
my $endline   = '^-'x80 . "\n\n";

open INFILE, '<', $infile;
open MDFILE, '<', $mdfile;

open INFO, '>', 'INFOFILE';
print INFO 'Hostname: ', `hostname`,
	   `hostname`,
	   "\n",

	   'Commandline:', "\n",
	   $startline,
	   $commandLine, "\n",
	   $endline,

	   'Environment:', "\n",
	   $startline,
           join("\n", map {"$_=$ENV{$_}"} sort(keys(%ENV))), "\n",
	   $endline,

	   'Flagsfile: ', $flagsfile, "\n",
	   $startline,
	   $flags,
	   $endline,

	   'Infile: ', $infile, "\n",
	   $startline,
	   <INFILE>,
	   $endline,

	   'MDfile: ', $mdfile, "\n",
	   $startline,
	   <MDFILE>,
	   $endline,

	   'Cliste:  ', $bin_cliste,  "\t", 'Rev: ', $rev, "\n",

close MDFILE,
close INFILE;
close INFO;

$ENV{EQI_INFOFILE} = 'INFOFILE';
#================================================================
# Run for every time point

system('/bin/cp', 'fort.17', 'fort.17.prepare');
my $fort_fmp = 'fort.17';


open F17, '<', $fort_fmp
    or die "Cannot read $fort_fmp: $!";

my $fort17 = <F17>;
close F17;


my ($header, $fmpdata) = split(qr{\n/ENDINV\s*=+\n}, $fort17);
my @fmpdata = split(qr{\n /EOF\s*-+\n}, $fmpdata);
pop @fmpdata;
if (! @fmpdata) {
    die "No times to do";
}
my $nt = scalar @fmpdata;
for my $i (1..$nt) {
    open FMP, '>', sprintf('fmpdata.%05d', $i);
    print FMP $flags, $header, "\n", $fmpdata[$i-1], "\n";
    close FMP;
}

#Same procedure for kinetic data
#check for fort.31 file
print $ifxtr,"\n";
if (-e 'fort.31') {
    $ifxtr = 1;
}
print $ifxtr,"\n";

if ($ifxtr) {
    open F31, '<', 'fort.31'
	or die "No fort.31 file!";
    my $fort31 = <F31>;
    close F31;
     
    my ($header, $xtrdata) = split(qr{\n/ENDINV\s*=+\n}, $fort31);
    my @xtrdata = split(qr{\n/EOF\s*=+\n}, $xtrdata);
    pop @xtrdata;
    if (! @xtrdata) {
	die "No times to do";
    }
    my $nt = scalar @xtrdata;
    for my $i (1..$nt) {
	open XTR, '>', sprintf('xtrdata.%05d', $i);
	print XTR "1\n";
	print XTR $header, "\n", $xtrdata[$i-1], "\n";
	close XTR;
    }
}


# Start jobs
my @pid;

for my $ijob (1..$njobs) {

    mkdir($ijob);

    if ($pid[$ijob]= fork() ) {
	# parent: NOP
    }
    else {
        die "Cannot fork $ijob: $!" unless defined $pid[$ijob];
	chdir($ijob);

	system('/usr/bin/cpp',
	       '-P' => $infile,
	       'fort.13'
	);

	system('/usr/bin/cp', 
	       $mdfile,
	       'MDfile.xml'
	);

	foreach my $f (18,19,20,21,22,23,24,26,32) {
	    open F, '>', "fort.$f";
	    print F "0\n";
	    close F;
	}

	runAsChild($ijob);
	exit;
    }
}

# Wait for childs to finish

for my $ijob (1..$njobs) {
    waitpid($pid[$ijob], 0);
}

#Collect results

my @f8 = glob('fort.8.*');
if (! @f8) {
    die "No timepoint converged\n";
}

my $nSuccess = scalar @f8;

open INFILE, '<', $infile;
open RESULT, '>', 'result';
print RESULT  $startline,
    <INFILE>,
    $endline,
close INFILE;
print RESULT "N_EQUIL: ",$nSuccess,"\n";
foreach my $f8 (@f8) {
    open F8, '<', $f8;
    print RESULT "===============================\n",
		 <F8>;
    close F8;
}
close RESULT;

my @f56 = glob('fort.56.*');
if (@f56 != $nSuccess) {
    die "Inconsistent fort.56";
}

open EQDSK,  '>', 'eqdsk';
foreach my $f56 (@f56) {
    open  F56, '<', $f56;
    print EQDSK <F56>;
    close F56;
}


    
#=====================================================
# Grep censored probes

if ($grepcens) {
    print STDOUT '**** *** *** ***', "\n";
    print STDERR '**** *** *** ***', "\n";
    my $ident = $tmpdir;
    $ident =~ s{^.*/}{};
    my $file = '/afs/ipp/u/tech/CENS/' . $ident;
    my $output = qx{/afs/ipp/u/jcf/fp/grepcens.pl ./result};
    open CENS_LOG, '>>', $file
	or warn "Cannot write $file: $!";
    print CENS_LOG $output, "\n========================================================\n\n";
    close CENS_LOG;
    print STDOUT '**** *** *** ***', "\n";
    print STDERR '**** *** *** ***', "\n";
}

#=====================================================
sub runAsChild {
    my $ijob   = shift;

    open STDOUT, '>', 'stdout';
    open STDERR, '>', 'stderr';

    print STDERR scalar(localtime), ' ', "Job $ijob starting\n";
    # Try to run commands.
    my @fmpFiles = glob('../fmpdata.*');
    while (1) {

	last unless @fmpFiles;

	my $fmpFile = shift @fmpFiles;
	if (! rename($fmpFile, 'fort.15')) {
	    # Possible race condition, already caught by another process
	    print STDERR "Trying to catch $fmpFile failed\n";
	    next;
	}

	$fmpFile =~ /(\d+)/;
	my $it = $1;
	my $xtrFile = '../xtrdata.'.$it;
	if ($ifxtr) {
	    rename($xtrFile,'fort.31');
	}else{
	    open F, '>', "fort.31";
	    print F "0\n";
	    close F;
	}

	print STDERR scalar(localtime()), ' ', 'Starting cliste', "\n";
        system($bin_cliste);
	print STDERR scalar(localtime()), ' ', 'Finished cliste', "\n";
	system('/bin/ls -Flags');

	if (! -e 'fort.8' || -z _ || -s _ < 200) {
	    print STDERR scalar(localtime()), ' ', 'fort.8 (nearly) empty', "\n";
	}
	elsif (! -e 'fort.56' || -z _ || -s _ < 200) {
	    print STDERR scalar(localtime()), ' ', 'fort.56 (nearly) empty', "\n";
	}
	else {
	    print STDERR "SUCCESS $ijob $it\n";
	    my $c = sprintf('%05d', $it);
	    rename("fort.56", "../fort.56.$c") or die $!;
	    rename("fort.8",  "../fort.8.$c")  or die $!;
	}

	# unlink('core');

    }

    exit;
}

sub printUsage {
    print STDERR <<_EOF_;
Usage: $0 [options] shot

Options:
    General:

    --tmpdir dir        directory where to run
    --[no]core          (do not) allow core files (default: no)

    Running cliste:

    --bin_cliste exe    executable for cliste
                        (default: $bin_cliste)
    --infile file       infile
                        (default: $infile)
    --flagsfile file    std flags file
                        (default: $flagsfile)
    --mdfile            MD file
                        (default: $mdfile)
    --njobs n           number of parallel jobs

    Writing shotfiles:

    --[no]grepcencs     (do not) run grepcens (default: run)


_EOF_

exit 1;
}
