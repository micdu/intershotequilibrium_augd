import aug_sfutils as dd
import numpy as np
import sys
from utils.sgroup_augww import sgroup_write_augsf

def moving_average(x,n=2):
    av = np.cumsum(x,dtype=float)
    av[n:] = av[n:] - av[:-n]
    return av[n-1:] / n


class SignalGroup:
    def __init__(self,snames, data, timebase):
        self.snames = snames
        self.data = data
        self.timebase = timebase

def makeMBI(shot,write=True):
    diag='MAY'
    exp='AUGD'

    signals = ['BTF','IV1o','IV1u','IV2o','IV2u','IV3o','IV3u',
               'ICoIo','ICoIu','IOH','dIOH2s','dIOH2u']
    
    data = []
    sf=dd.SFREAD(diag,shot)
    time=sf.gettimebase(signals[0])

    toff = np.arange(100)/99.*0.1 - 9.9
    for sig in signals:
        temp=sf.getobject(sig)
        offset = np.mean(np.interp(toff,time,temp))
        data.append(temp-offset)
        
    itf_data = sf.getobject('ITFABB')
    btf_data = sf.getobject('BTF')
    sigs = SignalGroup(signals,data,time)
 
    #set up factor to convert from current to field measurement
    mu0 = np.pi * 4e-7
    R0 = 1.65
    ncoils=16
    nturns=24
    factor = mu0 * ncoils * nturns / (2. * np.pi * R0)

    #Find sign of Bt and determine smoothing period
    signbt = np.sign(np.interp(-3.5,time,btf_data))
    dt_min = 0.0
    if ((shot >= 30136) & (shot < 30461)):
        print('Smoothing over 10 ms!!')
        dt_min=0.01
        btf_abb = factor * np.abs( moving_average( itf_data ) ) * signbt
    else:
        btf_abb = factor * itf_data
        
    signabb = np.sign(np.interp(-3.5,time,btf_data))
    if signabb != signbt:
        btf_abb = -1 * btf_abb

    signals.append('BTFABB')
    data.append(btf_abb)
    data = np.array(data).transpose()
    
    sigs = SignalGroup(signals,data,time)
        
    if write:
        print('Writing MBI SF')
        expW = 'AUGD'
        diagW='MBI'
        tb_name = 'timeB'
        sgroup_write_augsf(sigs,shot,diagW,tb_name=tb_name,experiment='AUGD')
        #dd.ww.write_sf(shot,data,'./SFH',diagW,exp=expW)
    else:
        print('Not writing MBI shotfile')

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('shot',type=int)
    args = parser.parse_args()

    makeMBI(args.shot)
