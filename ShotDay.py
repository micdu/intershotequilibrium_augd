import aug_sfutils as dd
import numpy as np
import sys,time
import subprocess as sp

timeout_may = 20*60 #wait 20 minutes before abandoning
timeout_wait = 60
def CheckSF(shot,diag):
    p = sp.Popen(["shotls",diag,"%d"%shot],
                 stdout=sp.PIPE,stderr=sp.PIPE)
    out = p.stdout.readlines()
    if out:
        status = True
    else:
        status = False
    return status

while True:
    shot = dd.wait4shot()
    print(shot)
    print('Now executing script')
    Done = False
    wait = 0
    while not Done:
        if wait > 10:
            Done = True
            print('Timed out while waiting for MAY')
        MAYwritten = CheckSF(shot,'MAY')
        if MAYwritten:
            sp.run(["./run_allintershot.sc","%d"%shot])
            Done = True
            wait = 0
        else:
            print('waiting')
            time.sleep(timeout_wait)
            wait += 1
            
    
