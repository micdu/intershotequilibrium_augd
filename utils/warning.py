import tkinter as tk  # popup window



def warning(input_text):
    print('Running warning')
    reg_font = ("Verdana", 22)
    msg = "Something is missing from intershot-EQ!"
    popup = tk.Tk()
    
    popup.minsize(800, 600) #
    
    popup.wm_title("Warning")
    popup.geometry("500x500")
    text = tk.Label(popup, text=msg, font=reg_font)
    text.pack(side="top")
    b1 = tk.Button(popup, text=input_text, command=popup.destroy)
    
    b1.config(height = 20, width = 150) #
  
    
    b1.pack()
    popup.mainloop()
