#!/usr/bin/env python
#  
# fpcof.py - FP coefficients class
#            makes netcdf file for given ascii files if called as main program

#  07-Feb-2017    wls      new
#  14-Dec-2018    micdu    split flimcof into [io]flimcof
#  17-Nov-2020    wls      python3
#  25-May-2022    wls      added ERSprobenames

import numpy as np
import netCDF4

# import pdb

# ============= subroutines ===========================

def JoinProbenames(probenamesets):
  '''
    For a given tuple of probename sets (arrays,lists), find the 
    unique union of all probenames and compile a tuple with 
    an index list for each input probename set.
    Input parameter:
       probenamesets   tuple of (np.array or list) of probe names
    Returns:
       probenames      np.array of unique probe names
       ixx             tuple of np.arrays of indices, positions as in input parameter
  '''
  probenames = ['ONE']
  ixx = ()
  for i, pns in enumerate(probenamesets):
    # print(pns)
    ix = []
    for pn in pns:
      if pn not in probenames:
        probenames.append(pn)
      ix.append(probenames.index(pn))
    ixx = ixx + (np.array(ix),)
  return np.array(probenames), ixx


# read list of names from ascii file
def read_ascii_names(fn):
  f = open(fn, 'r')
  t1=str.maketrans('','','{}" \r\n')
  strlist = []
  for l in f:
    l1 = l.translate(t1)
    l2 = l1.rstrip(',')
    l3 = l2.split(',')
    for s in l3:
      strlist.append(s)
  f.close()
  return strlist

# read numerical data from ascii file (into flat 1D array)
def read_ascii_realnum(fn):
  f = open(fn, 'r')
  t1=str.maketrans('','','{}" \r\n')
  data = np.array([])
  for l in f:
    l1 = l.translate(t1)
    l2 = l1.rstrip(',')
    l3 = l2.split(',')
    for s in l3:
      data = np.append(data, float(s))
  f.close()
  return data


# ==========  FP coefficients class  ===============

class FPcoefficients:

# --- init empty set
  def __init__ (self):
    self.have_IPcof = False
    self.have_FPprobdat = False
    self.have_erscof = False
    self.have_FPCcof = False
    self.have_FPGcof = False
    pass

# --- read from netcdf file
  @classmethod
  def from_netcdf (cls, fn):
    c = cls()
    f = netCDF4.Dataset(fn, 'r')
    # .... IP recovery ........
    c.IPprobenames = (f.variables['IPprobenames'])[:]
    c.niprob = len(c.IPprobenames)
    c.ipcof = np.asmatrix((f.variables['ipcof'])[:]).T
    c.have_IPcof = True
    # ..... FP probe data .....
    c.FPprobenames = (f.variables['FPprobenames'])[:]
    c.nfprob = len(c.FPprobenames)
    c.vmin = (f.variables['vmin'])[:]
    c.vmax = (f.variables['vmax'])[:]
    # ..... sensor replacement ("Ersatz") data ......
    c.erscof = (f.variables['erscof'])[:]
    c.nerscof = c.erscof.shape[0]
    try:
      c.ERSprobenames = (f.variables['ERSprobenames'])[:]
    except:
      if c.nerscof == c.nfprob+2:   # convention: PSL currents at the end
        c.ERSprobenames = list(c.FPprobenames).extend(['Ipslon','Ipslun'])
      elif c.nerscof == c.nfprob:
        c.ERSprobenames = c.FPprobenames
      else:    # no idea what it could be so assume nothing
        c.ERSprobenames = None
    c.have_erscof = True
    # ..... Eigenvector projection .......
    c.areavec = np.array( (f.variables['areavec'])[:] )
    c.vmean   = np.array( (f.variables['vmean'])[:] )
    c.sval    = np.array( (f.variables['sval'])[:] )
    c.evec    = np.asmatrix( (f.variables['evec'])[:] )
    c.npc = c.evec.shape[1]
    c.have_FPprobdat = True
    # ..... FPC recovery ......
    c.FPCprobenames = (f.variables['FPCprobenames'])[:]
    c.nfpcprob = len(c.FPCprobenames)
    c.FPCnames = (f.variables['FPCnames'])[:]
    c.fpccof = np.asmatrix( (f.variables['fpccof'])[:] )
    c.nfpc = c.fpccof.shape[1]
    c.nfpccof = c.fpccof.shape[0]
    c.have_FPCcof = True
    # ...... Limiter fluxes regression ......
    try:
      c.LIMprobenames = (f.variables['LIMprobenames'])[:]
    except:
      c.LIMprobenames = None
    c.iflimcof = np.asmatrix( (f.variables['iflimcof'])[:] )
    c.oflimcof = np.asmatrix( (f.variables['oflimcof'])[:] )
    c.niflim = c.iflimcof.shape[1]
    c.noflim = c.oflimcof.shape[1]
    # ...... X-pt fluxes regression .........
    c.fxocof = np.asmatrix( (f.variables['fxocof'])[:] ).T
    c.fxucof = np.asmatrix( (f.variables['fxucof'])[:] ).T
    c.nfpgcof = c.fxocof.shape[0]
    # ..... FPG recovery .....
    c.FPGnames = (f.variables['FPGnames'])[:]
    c.fpgcof = (f.variables['fpgcof'])[:]
    c.nkat = c.fpgcof.shape[0]
    c.nfpg = c.fpgcof.shape[1]
    c.have_FPGcof = True
    # ..... Postprocessing lists ........
    c.Postprocessing = {}
    try:
      post = (f.variables['Postprocessing'])[:]
      for p in post:
        p1 = (f.variables['Post_'+p])[:]
        c.Postprocessing[p] = p1
    except:
      print('No/inconsistent postprocessing rules found in FP coefficients set.')
    f.close()
    return c

# --- read from a set of ascii files
  @classmethod
  def from_ascii (cls, directory):
    c = cls()
    # .... IP recovery ........
    c.IPprobenames = read_ascii_names(directory+'/IPprobenames.txt')
    niprob = len(c.IPprobenames)
    c.niprob = niprob
    ipcof_file = read_ascii_realnum(directory+'/ipcof.txt')
    c.ipcof  = np.asmatrix(ipcof_file[0:niprob]).T
    c.have_IPcof = True
    # ..... FP probe data .....
    c.FPprobenames = read_ascii_names(directory+'/FPprobenames.txt')
    nfprob = len(c.FPprobenames)
    c.nfprob = nfprob
    c.nerscof = nerscof
    erscof_file = read_ascii_realnum(directory+'/erscof.txt')
    c.vmin = erscof_file[0:nfprob]          # minimum values
    c.vmax = erscof_file[nfprob:2*nfprob]   # maximum values
    # ..... sensor replacement ("Ersatz") data ......    
    c.erscof = np.transpose(np.reshape(erscof_file[2*nfprob:],(nfprob,nfprob)))  # replacement matrix
    c.ERSprobenames = c.FPprobenames
    c.erscof = True
    # ..... Eigenvector projection .......
    evec_file = read_ascii_realnum(directory+'/evec.txt')
    c.areavec = np.asmatrix( evec_file[0:nfprob] ).T
    c.vmean   = np.asmatrix( evec_file[nfprob:2*nfprob] ).T
    c.sval    = np.asmatrix( evec_file[2*nfprob:3*nfprob] ).T
    evec = np.asmatrix( np.reshape( evec_file[3*nfprob:], (nfprob,nfprob)) ).T
    npc = 20
    c.npc = npc
    c.evec = evec[:,0:npc]
    c.have_FPprobdat = True
    # ..... FPC recovery ......
    c.FPCprobenames = c.FPprobenames + ['Ipslun', 'Ipslon']
    nfpccof = nfprob+3   # intercept + nfprob probes + Ipsluk, Ipslok
    c.nfpccof = nfpccof
    c.FPCnames = read_ascii_names(directory+'/FPCnames.txt')
    nfpc = len(c.FPCnames)
    c.nfpc = nfpc
    fpccof_file = read_ascii_realnum(directory+'/fpccof.txt')
    c.fpccof = np.asmatrix( np.reshape(fpccof_file,(nfpc,nfpccof)) ).T
    c.have_FPCcof = True
    # ...... Limiter fluxes regression ......
    c.LIMprobenames = ['ONE'] + c.FPprobenames 
    limflcof = read_ascii_realnum(directory+'/limflcof.txt')
    nlimfl = int(limflcof.size/(nfprob+1))
    flimcof  = np.asmatrix( np.reshape(limflcof,(nlimfl,nfprob+1)) ).T  # limiter flux differences
    c.iflimcof = flimcof[:,0:6]
    c.oflimcof = flimcof[:,6:]
    # ...... X-pt fluxes regression .........
    xpfcof = read_ascii_realnum(directory+'/xpfcof.txt')
    nfpgcof = int((npc+1)*(npc+2)/2)
    if xpfcof.size != 2*nfpgcof:
      print('Inconsistent number of entries in xpfcof for %d principal components' % npc)
    c.nfpgcof = nfpgcof
    c.fxocof = np.asmatrix (xpfcof[0:nfpgcof]).T
    c.fxucof = np.asmatrix (xpfcof[nfpgcof:2*nfpgcof]).T
    # ..... FPG recovery .....
    c.FPGnames = read_ascii_names(directory+'/fpgnames.txt')
    nfpg = len(c.FPGnames)
    c.nfpg = nfpg
    fpgcof = read_ascii_realnum(directory+'/fpgcof.txt')
    # fpgcof format in ...ascii file: [nkat, nfpg, nfpgcof], ... this object: [nkat, nfpgcof, nfpg]
    c.fpgcof = np.swapaxes(np.reshape(fpgcof, (4, nfpg, nfpgcof)), 1, 2)
    c.have_FPGcof = True
    # no postprocessing definition in ascii files
    c.Postprocessing = {}
    return c

# --- save to netcdf file
  def save_netcdf(self, fn, description = ''):
    probenamesets = ()
    file = netCDF4.Dataset(fn, 'w', format='NETCDF4')
    file.description = description
#    file.createDimension('strlen', 10)  # max. number of characters in a string
    if self.have_IPcof:
      file.createDimension('niprob', self.ipcof.shape[0])  # number of probes for current determination
    if self.have_FPprobdat:
      file.createDimension('nfprob', self.vmin.shape[0])   # number of FPC probes
      file.createDimension('npc', self.evec.shape[1])         # number of principle components
    if self.have_erscof:
      file.createDimension('nerscof', self.erscof.shape[0])   # number measurements for erscof
    if self.have_FPCcof:
      file.createDimension('nfpc', self.fpccof.shape[1])   # number of FPC quantities
      file.createDimension('nfpccof', self.fpccof.shape[0])  # FPC (linear) input vector length
      file.createDimension('nfpcprob', len(self.FPCprobenames))  # number of measurements for FPC
    if self.have_FPGcof:      
      file.createDimension('nflimprob', self.iflimcof.shape[0])  # limiter flux probes
      file.createDimension('niflim',self.iflimcof.shape[1])
      file.createDimension('noflim',self.oflimcof.shape[1])
      file.createDimension('nxptcof',self.fxucof.shape[0])    # X-point coeffs
      file.createDimension('nfpgcof', self.fpgcof.shape[1])  # FPG (nonlinear) input vector length
      file.createDimension('nfpg', self.fpgcof.shape[2])  # number of FPG quantities
      file.createDimension('nkat', self.fpgcof.shape[0])  # number of FPG kategories
    if self.have_IPcof:
      nc_IPprobenames = file.createVariable('IPprobenames', str, ('niprob'))
      nc_IPprobenames[:] = np.array(self.IPprobenames, dtype='object')
      probenamesets = probenamesets + (self.IPprobenames, )
      nc_ipcof = file.createVariable('ipcof', 'f8', ('niprob'))
      nc_ipcof[:] = np.array(self.ipcof)[:,0]
    if self.have_FPprobdat:
      nc_FPprobenames = file.createVariable('FPprobenames', str, ('nfprob'))
      nc_FPprobenames[:] = np.array(self.FPprobenames, dtype='object')
      probenamesets = probenamesets + (self.FPprobenames, )
      nc_vmin = file.createVariable('vmin', 'f8', ('nfprob'))
      nc_vmax = file.createVariable('vmax', 'f8', ('nfprob'))
      nc_areavec  = file.createVariable('areavec', 'f8', ('nfprob'))
      nc_vmean    = file.createVariable('vmean', 'f8', ('nfprob'))
      nc_sval     = file.createVariable('sval', 'f8', ('nfprob'))
      nc_evec     = file.createVariable('evec', 'f8', ('nfprob','npc'))
      nc_vmin[:] = self.vmin
      nc_vmax[:] = self.vmax
      nc_areavec[:] = self.areavec[:,0]
      nc_vmean[:]   = self.vmean[:,0]
      nc_sval[:]    = self.sval[:,0]
      nc_evec[:,:]  = self.evec
    if self.have_erscof:
      nc_ERSprobenames = file.createVariable('ERSprobenames', str, ('nerscof'))
      nc_ERSprobenames[:] = np.array(self.ERSprobenames, dtype='object')
      probenamesets = probenamesets + (self.ERSprobenames, )
      nc_erscof = file.createVariable('erscof', 'f8', ('nerscof','nfprob'))
      nc_erscof[:,:] = self.erscof
    if self.have_FPCcof:
      nc_FPCnames = file.createVariable('FPCnames', str, ('nfpc'))
      nc_FPCnames[:] = np.array(self.FPCnames, dtype='object') 
      nc_FPCprobenames = file.createVariable('FPCprobenames', str, ('nfpcprob'))
      nc_FPCprobenames[:] = np.array(self.FPCprobenames, dtype='object') 
      probenamesets = probenamesets + (self.FPCprobenames, )
      nc_fpccof = file.createVariable('fpccof', 'f8', ('nfpccof','nfpc'))
      nc_fpccof[:,:] = self.fpccof
    if self.have_FPGcof:
      nc_FPGnames = file.createVariable('FPGnames', str, ('nfpg'))
      nc_FPGnames[:] = np.array(self.FPGnames, dtype='object')
      nc_LIMprobenames = file.createVariable('LIMprobenames', str, ('nflimprob'))
      nc_LIMprobenames[:] = np.array(self.LIMprobenames, dtype='object')
      probenamesets = probenamesets + (self.LIMprobenames, )
      nc_iflimcof = file.createVariable('iflimcof', 'f8', ('nflimprob','niflim'))
      nc_oflimcof = file.createVariable('oflimcof', 'f8', ('nflimprob','noflim'))
      nc_fxocof   = file.createVariable('fxocof', 'f8', ('nxptcof'))
      nc_fxucof   = file.createVariable('fxucof', 'f8', ('nxptcof'))
      nc_fpgcof   = file.createVariable('fpgcof', 'f8', ('nkat', 'nfpgcof', 'nfpg'))
      nc_iflimcof[:,:]  = self.iflimcof
      nc_oflimcof[:,:]  = self.oflimcof
      nc_fxocof[:]  = self.fxocof[:,0]
      nc_fxucof[:]  = self.fxucof[:,0]
      # print (self.fxucof.shape)
      # print (self.fpgcof.shape)
      nc_fpgcof[:]  = self.fpgcof

    # write postprocessing rules
    if hasattr(self, 'Postprocessing'):
      file.createDimension('npostproc', len(self.Postprocessing.keys())) 
      nc_Postproc = file.createVariable('Postprocessing', str, ('npostproc'))
      keys = [k for k in self.Postprocessing.keys()]
      nc_Postproc[:] = np.array(keys, dtype='object')        
      for k in self.Postprocessing.keys():
        kdim = 'npost_' + k
        file.createDimension(kdim, len(self.Postprocessing[k])) 
        nc_Postproc1 = file.createVariable('Post_'+k, str, (kdim))
        nc_Postproc1[:] = np.array(self.Postprocessing[k], dtype='object')          

    # create and write common set of probe names (and index arrays)
    if probenamesets != ():
      probenames, ixx = JoinProbenames(probenamesets)
      file.createDimension('nprob', len(probenames))    # number of probes (globally)
      nc_probenames = file.createVariable('probenames', str, ('nprob'))
      nc_probenames[:] = probenames
      i = 0
      if self.have_IPcof:
        nc_IPprobeindices = file.createVariable('IPprobeindices', 'i4', ('niprob'))
        nc_IPprobeindices[:] = ixx[i]
        i += 1
      if self.have_FPprobdat:
        nc_FPprobeindices = file.createVariable('FPprobeindices', 'i4', ('nfprob'))
        nc_FPprobeindices[:] = ixx[i]
        i += 1
      if self.have_erscof:
        nc_ERSprobeindices = file.createVariable('ERSprobeindices', 'i4', ('nerscof'))
        nc_ERSprobeindices[:] = ixx[i]
        i += 1
      if self.have_FPCcof:
        nc_FPCprobeindices = file.createVariable('FPCprobeindices', 'i4', ('nfpcprob'))
        nc_FPCprobeindices[:] = ixx[i] 
        i += 1
      if self.have_FPGcof:      
        nc_LIMprobeindices = file.createVariable('LIMprobeindices', 'i4', ('nflimprob'))
        nc_LIMprobeindices[:] = ixx[i]
        i += 1
      
    file.close()

  # ========================== FP evaluation ====================================

  def indices_to_signals_and_probes(self, signames, probenames, cofsetname):
    '''
       for a list of FP 'probenames', find corresponding signals (given by 'signames')
       'cofsetname' identifies the coefficient set name (used only for error output)
       If no correspondence for a required probe can be found, an exception is raised.

       return values:
       ixsig     array of indices to signals (input data for FP analysis)
       ixprob    array of indices to probes (FP coefficients)
       ixoffs    index (scalar) to FP entry for offset or None if no offset specified
    '''
    ixsig = []
    ixprob = []
    ixoffs = None
    for i, pname in enumerate(probenames):
      iix = np.where(signames==pname)[0]  # find in signals list
      if len(iix)==0:
        if pname == 'ONE':
          ixoffs = i          # offset FP coefficient
        else:
          print("%s: Cannot find required FP input signal %s" % (cofsetname, pname))
          print(signames)
      else:
        ixprob.append(i)      # probe FP coefficient
        ixsig.append(iix[0])  # index to signal array

    if   (ixoffs is None and len(ixsig) != len(probenames)) \
      or (ixoffs is not None and len(ixsig)+1 != len(probenames)):
        raise Exception ("%s: Not all required signals found - giving up." % cofsetname)

    return np.array(ixsig), np.array(ixprob), ixoffs



  def linear_evaluation(self, signals, signames, fpcof, probenames, cofsetname, variance=None):
    '''
        linear FP analysis for 'ns' 'signals' with names 'signames'

        input parameters:
            signals (nt, ns)    numpy array of input signals
            signames (ns)       array of signal names (to identify the signals)
            fpcof (nprob, nfp)  linear FP coefficient rable
            cofsetname          name of coefficient set (for error ouput only)
        optional input parameters:
            variance (ns)       variance of each signal, used for error estimation
        return values:
            result(nt, nfp)     linear FP evaluation result
            noise(nfp)          noise estimate (if variance is given) or None
    '''
    ixsig, ixprob, ixoffs = self.indices_to_signals_and_probes(signames, probenames, cofsetname)
    if ixoffs is None:
      result = np.dot(signals[:,ixsig], fpcof[ixprob, :])
    else:
      result = fpcof[ixoffs,:] + np.dot( signals[:,ixsig], fpcof[ixprob, :])
        
    if variance is not None:   # assume offset does not contribute to noise
        noise = np.array( np.sqrt( np.asmatrix(variance[ixsig]) * np.square(fpcof[ixprob,:])))
        return result, noise
    return result, None


  def IP_evaluation(self, signals, signames, variance=None):
    '''
        estimate plasma current for a set of "ns" signals, each at "nt" times (or cases).
    '''
    if not self.have_IPcof:
      print("fpcof.py: No IP coefficients available.")
      return None, None
    return self.linear_evaluation(signals, signames, \
              self.ipcof, self.IPprobenames, "fpcof - IP evaluation", variance=variance)


  def FPC_evaluation(self, signals, signames, variance=None, verbose=True):
    '''
        estimate FPC parameters (Rcurr, Zcurr) for a set of "ns" signals, each at "nt" times (or cases).
        return values:
            result(nt, nfpc)
            noise(nfpc)       Noise estimate or None if variance not given
    '''
    if not self.have_FPCcof:
      print("fpcof.py: No FPC coefficients available.")
      return None, None
    cofsetname = "fpcof - FPC evaluation"
    ixsig, ixprob, ixoffs = self.indices_to_signals_and_probes(signames, self.FPCprobenames, cofsetname)
    result, noise = self.linear_evaluation(signals, signames, \
              self.fpccof, self.FPCprobenames, "fpcof - FPC evaluation", variance=variance)

    # obsolete way to find quadratic signals and calculate square root
    #for i, rname in enumerate(self.FPCnames):
    #  if rname in ['Rslin', 'Rsqlin', 'Rcurr', 'Rcurr2', 'Rs2', 'Rsqlinnopsl']:
    #    result[:,i] = np.sqrt(result[:,i])
    #    if noise is not None:
    #      noise[i] = np.sqrt(noise[i])

    # sqrt() postprocessing  (Rslin)
    if hasattr(self, 'Postprocessing'):
      if 'sqrt' in self.Postprocessing.keys():
        for i, FPCname in enumerate(self.FPCnames):
          if FPCname in self.Postprocessing['sqrt']:
            if verbose:
              print("sqrt() postprocessing of quantity %s" % FPCname)
            wh = np.where(result[:,i] < 0)[0]
            if len(wh)>0:
              if verbose:
                print("  %d observations have negative values -> set to zero." % len(wh))
              result[wh,i] = 0.0
              if noise is not None:
                noise[i] = 0.0
            result[:,i] = np.sqrt(result[:,i])
            if noise is not None:
              noise[i] = np.sqrt(noise[i])          
    return result, noise

  
  def ERS_evaluation(self, signals, signames, variance=None):
    '''
      find probe replacements for all FP probes
    '''
    if not self.have_erscof:
      print("fpcof.py: No probe replacement coefficients available.")
      return None, None
    result, noise = self.linear_evaluation(signals, signames, \
              self.erscof, self.ERSprobenames, "fpcof - FPC evaluation", variance=variance)
    return result, noise

  
  def LIM_evaluation(self, signals, signames, variance=None):
    '''
      evaluate inner and outer limiter fluxes

      returns:
        IL_result, OL_result, IL_noise, OL_noise
      where
        IL/OL_result:    flux values (Vs) at inner/outer limiter points
        IL/OL_noise:     propagated IL/OL noise levels (if variance is given) or None
    '''

    if not self.have_FPGcof:
      print("fpcof.py: No FPG (limiter flux) coefficients available.")
      return None, None, None, None
    IL_result, IL_noise = self.linear_evaluation(signals, signames, \
              self.iflimcof, self.LIMprobenames, "fpcof - inner limiter flux evaluation", variance=variance)
    OL_result, OL_noise = self.linear_evaluation(signals, signames, \
              self.oflimcof, self.LIMprobenames, "fpcof - outer limiter flux evaluation", variance=variance)
    return IL_result, OL_result, IL_noise, OL_noise
    

  
  def quadratic_combinations(self, signals, signames, variance=None):
    '''
      produce a signal group with quadratic combinations of principle components of input signals
    '''

    if not self.have_FPGcof:
      print("fpcof.py: No FPG (quadratic components) coefficients available.")
      return None, None
    
    ixsig, ixprob, ixoffs = self.indices_to_signals_and_probes(signames, self.FPprobenames, "FP Quadratic combinations")
    # ignore ixoffs, i.e. we assume the first FP coefficient is an offset anyway

    npc = self.evec.shape[1]  # number of PCs
    ncases = signals.shape[0]

    # normalise selected signals to probe area and substract database mean value
    vc = (signals[:,ixsig] / np.outer(np.ones((ncases,1)), self.areavec)) \
          - np.outer(np.ones((ncases,1)), self.vmean)
    # project on eigenvectors = principle components (PCs)
    X = np.dot(vc, self.evec)
    # prepend with unity vector
    q = np.column_stack((np.ones((ncases, 1)), X))

    if variance is not None:
      # noise (variance) for each probe [ip]
      vcv1 = (variance[ixsig] / self.areavec)
      # replicate identically for each observation [io,:]) 
      vcv = np.outer(np.ones((ncases, 1)), vcv1)
      # noise (variance) for each each eigenvector [:,ie] (still the same for each observation [io,:])
      Xv = np.array( np.asmatrix(vcv) * np.square(self.evec) )
      # prepend with unity vector
      qv = np.column_stack((np.zeros((ncases, 1)), Xv))

    # add quadratic combinations
    Xa = np.array(X)  # for element-wise multiplication below
    for j in range(npc):
      # multiply this PC with itself and all PCs to come
      x21 = np.outer(Xa[:,j], np.ones((1,npc-j))) * Xa[:,j:]
      # and append to the result
      q = np.column_stack((q, x21))
      if variance is not None:
        xv21 = np.outer(np.square(Xa[:,j]), np.ones((1,npc-j))) * Xv[:,j:] \
            + np.outer(Xv[:,j], np.ones((1,npc-j))) * np.square(Xa[:,j:]) \
            + np.outer(Xv[:,j], np.ones((1,npc-j))) * Xv[:,j:]  
        qv = np.column_stack((qv, xv21))
    
    if variance is not None:
      return q, qv
    return q, None


  def Xpt_evaluation(self, signals, signames, variance=None):
    '''
      evaluate X-point fluxes

      returns:
        XU_result, XO_result, XU_noise, XO_noise
      where
        XU/XO_result:    flux values (Vs) at lower/upper X-point
        XU/XO_noise:     propagated XU/XO noise levels (if variance is given) or None
    '''
    if not self.have_FPGcof:
      print("fpcof.py: No FPG (Xpt evaluation) coefficients available.")
      return None, None, None, None

    q, qv = self.quadratic_combinations(signals, signames, variance=variance)
    psiXu = np.dot(q, self.fxucof)
    psiXo = np.dot(q, self.fxocof)

    if variance is None:
      return psiXu, psiXo, None, None

    # as qv depends on observation, so does resulting variance
    XUvariance = np.array( np.asmatrix(qv) * np.square(self.fxucof) )
    XOvariance = np.array( np.asmatrix(qv) * np.square(self.fxocof) )
    # noise is averaged over all observations
    XUNoiseMean = np.sqrt( np.mean(XUvariance, axis=0) )
    XONoiseMean = np.sqrt( np.mean(XOvariance, axis=0) )
    return psiXu, psiXo, XUNoiseMean, XONoiseMean


  def category_evaluation(self, signals, signames):
    '''
      find plasma category

      returns:
        ikCAT
    '''
    if not self.have_FPGcof:
      print("fpcof.py: No FPG (category evaluation) coefficients available.")
      return None, None, None, None

    # look up current centroid radius
    FPC, dummy1 = self.FPC_evaluation(signals, signames)
    for i, rname in enumerate(self.FPCnames):
      if rname in ['Rslin', 'Rsqlin', 'Rcurr', 'Rcurr2', 'Rs2', 'Rsqlinnopsl']:
        Rcurr = np.array(FPC)[:,i]
        break
    # limiter and X-pt fluxes
    iflim, oflim, dummy1, dummy2 = self.LIM_evaluation(signals, signames)
    psiXu, psiXo, dummy1, dummy2 = self.Xpt_evaluation(signals, signames)

    # build array of boundary candidates
    iflim_max = np.amax(iflim, axis=1)    # maximum flux inner limiter
    oflim_max = np.amax(oflim, axis=1)    # maximum flux outer limiter
    psi_bdrycand = np.array( np.column_stack( (iflim_max, oflim_max, psiXo, psiXu) ) )

    # candidate with maximum flux determines category
    ikCat = np.argmax(psi_bdrycand, axis=1) + 1

    # heuristic fixes for very small and very large current centroid radius
    wh_i = (Rcurr < 1.45)
    if np.any(wh_i):
      ikCat[wh_i] = 1
    wh_o = (Rcurr > 1.85)
    if np.any(wh_o):
      ikCat[wh_o] = 2

    return ikCat


  
  def FPG_evaluation(self, signals, signames, ikCat, variance=None, verbose=True):
    '''
      evaluate FPG quantities

      arguments:
         signals, signames   as above
         ikCat     plasma category 1(IL), 2(OL), 3(USN), 4(LSN)
                   either as a scalar (all observations use same FPG coefficients for this category)
                   or an array (length= #observations) each observation has its own category

    '''
    if not self.have_FPGcof:
      print("fpcof.py: No FPG coefficients (quadratic quantities evaluation) available.")
      return None, None

    q, qv = self.quadratic_combinations(signals, signames, variance=variance)
    FPGnoise = None
    
    if isinstance(ikCat,int):   # same category for all observations
      fpgquants = np.dot(q, self.fpgcof[ikCat-1,:,:])
      if variance is not None:
        FPGvariance = np.array( np.asmatrix(qv) * np.square(self.fpgcof[ikCat-1,:,:]) )
        FPGnoise = np.sqrt( np.mean(FPGvariance, axis=0) )

    elif len(ikCat) == signals.shape[0]:  # individual categories for observations
      fpgquants = np.zeros((signals.shape[0], self.fpgcof.shape[2]))
      if variance is not None:
        FPGnoise = fpgquants  # noise has same format as data
      for i in range(self.nkat):    # iterate by category
        wh_kat = (ikCat == i+1)
        if np.any(wh_kat):
          fpgquants[wh_kat,:] = np.dot(q[wh_kat,:], self.fpgcof[i,:,:])
          if variance is not None:
            FPGvariance = np.array( np.asmatrix(qv[wh_kat,:]) * np.square(fpgcof[i,:,:]) )
            FPGnoise[wh_kat,:] = np.sqrt( np.mean(FPGvariance, axis=0) )
            
    else:
      raise Exception ("fpcof.FPG_evaluation: ikCat must be a scalar or array of length %d" % (signals.shape[0]))

    # sqrt() postprocessing  (interformeter path lengths)
    if hasattr(self, 'Postprocessing'):
      if 'sqrt' in self.Postprocessing.keys():
        for i, FPGname in enumerate(self.FPGnames):
          if FPGname in self.Postprocessing['sqrt']:
            if verbose:
              print("sqrt() postprocessing of quantity %s" % FPGname)
            wh = np.where(fpgquants[:,i] < 0)[0]
            if len(wh)>0:
              if verbose:
                print("  %d observations have negative values -> set to zero." % len(wh))
              fpgquants[wh,i] = 0.0
              if FPGnoise is not None:
                FPGnoise[i] = 0.0
            fpgquants[:,i] = np.sqrt(fpgquants[:,i])
            if FPGnoise is not None:
              FPGnoise[i] = np.sqrt(FPGnoise[i])
    
    return fpgquants, FPGnoise

  

# ============== main program ======================

# make netcdf file from ascii coefficients provided by pjm
# if True:
  # directory = '../pjm_161229'
  #directory = '/afs/ipp-garching.mpg.de/u/wls/d/FP/pjm_161229'
  #fp = FPcoefficients.from_ascii(directory)
  #fp.save_netcdf('../fpcof/fpcof_pjm161229.nc')

if __name__ == '__main__':
      import sgroup
      MAG = sgroup.SignalGroup.from_netcdf('../FP_eqdb/2024A/independent.nc')
      PFC = sgroup.SignalGroup.from_netcdf('../FP_eqdb/2024A/PFcurrents.nc')

      signames = np.hstack((MAG.snames, PFC.snames))
      signals = np.hstack((MAG.data, PFC.data))
      
      # fp = FPcoefficients.from_netcdf('../FP_coefficients/fpcof_2023A_md2024Feb08b_FPCDivIIo.nc')
      fp = FPcoefficients.from_netcdf('../FP_coefficients/fpcof_2024A_md2024Feb08b_pathlen2.nc')

      IP, IPnoise = fp.IP_evaluation(signals, signames)
      FPC, FPCnoise = fp.FPC_evaluation(signals, signames)
      ERS, ERSnoise = fp.ERS_evaluation(signals, signames)
      ILIM, OLIM, ILIMnoise, OLIMnoise = fp.LIM_evaluation(signals, signames)
      psiXu, psiXo, psiXu_noise, psiXo_noise = fp.Xpt_evaluation(signals, signames)

      ikCat = fp.category_evaluation(signals, signames)
      FPG, FPGnoise = fp.FPG_evaluation(signals, signames, ikCat)


      
