import numpy as np
import matplotlib.pyplot as plt
from IPython import embed


Rlim_low = np.array([ \
                      0.2887, 1.1357, 1.1383, 1.1408, 1.1431, 1.1450, 1.2466, 1.2500, 1.2588, 1.2664, \
                      1.2729, 1.2783, 1.2854, 1.2878, 1.2872, 1.2829, 1.2360, 1.2340, 1.2437, 1.2440, \
                      1.2440, 1.2799, 1.2799, 1.2804, 1.3225, 1.3256, 1.4590, 1.4629, 1.5541, 1.5553, \
                      1.5553, 1.5675, 1.5793, 1.5794, 1.6140, 1.6352, 1.6361, 1.6431, 1.6581, 1.6737, \
                      1.7021, 1.7075, 1.7155, 1.7274, 1.7389, 1.7521, 1.9944], dtype=np.float32)

Zlim_low = np.array([ \
                      -1.1698, -0.6580, -0.6579, -0.6585, -0.6597, -0.6615, -0.8198, -0.8273, -0.8422, -0.8575, \
                      -0.8733, -0.8895, -0.9229, -0.9569, -0.9740, -0.9871, -1.1234, -1.1280, -1.1418, -1.1424, \
                      -1.1431, -1.1230, -1.1000, -1.0981, -1.0635, -1.0624, -1.0624, -1.0642, -1.1732, -1.1764, \
                      -1.1840, -1.2090, -1.2090, -1.2077, -1.0784, -0.9993, -0.9970, -0.9720, -0.9368, -0.9093, \
                      -0.8633, -0.8569, -0.8499, -0.8432, -0.8397, -0.8264, -0.5502], dtype=np.float32)

Rlim_upp = np.array([0.3000,1.173,1.275,1.315,1.293,1.471,1.672,1.771,1.9])
Zlim_upp = np.array([1.2767,0.789,0.936,1.084,1.116,1.100,1.082,1.074,1.064])

Upper = {'Rlim':Rlim_upp,'Zlim':Zlim_upp}
Lower = {'Rlim':Rlim_low,'Zlim':Zlim_low}

class s_coord:

    def __init__(self,div='Lower',Plot=False):
        if div == 'Lower':
            Rlim_xml = Lower['Rlim']
            Zlim_xml = Lower['Zlim']
        elif div == 'Upper':
            Rlim_xml = Upper['Rlim']
            Zlim_xml = Upper['Zlim']
        else:
            print('Div structure not recognised, defaulting to lower IIb')
            Rlim_xml = Lower['Rlim']
            Zlim_xml = Lower['Zlim']

        self.plot = Plot
            
        ds_vert = np.hypot(Rlim_xml[1:] - Rlim_xml[:-1],
                           Zlim_xml[1:] - Zlim_xml[:-1])
        #caution: skip first point in s array, which is definied to be at -1 m
        s_vert = (np.append(0, np.cumsum(ds_vert)) - 1.)[1:]
        #wos definition:
        #s_vert = s_vert - s_vert[3]
        
        #interpolate onto finer grid
        #Linear interpolation is done below to refine, so pick a medium-fine grid..
        steps = int(np.max(s_vert) / 0.001)
        self.s_vert = np.linspace(0.,np.max(s_vert),num=steps)
        self.Rlim = np.interp(self.s_vert,s_vert,Rlim_xml[1:])
        self.Zlim = np.interp(self.s_vert,s_vert,Zlim_xml[1:])
        if self.plot:
            plt.plot(self.Rlim,self.Zlim)
        
    def refine_s(self, flux, psibnd, zero_crossing):
		# just do a linear interpolation to refine. (wrt. poloidal flux)
        sort = zero_crossing + np.argsort(flux[zero_crossing+[0,1]])
        R = np.interp(psibnd, flux[sort], self.Rlim[sort])
        Z = np.interp(psibnd, flux[sort], self.Zlim[sort])
        S = np.interp(psibnd, flux[sort], self.s_vert[sort])                
        #print(psibnd, flux[sort], self.s_vert[sort], S)
        return (R,Z,S)
		
        
    def find_s(self,flux,psibnd):

        zero_crossings = np.where(np.diff(np.sign(flux-psibnd)))[0]
        
        if zero_crossings.size == 0:
            return (np.nan, np.nan, np.nan, np.nan, np.nan, np.nan)        
        
        Runi, Zuni, Suni = self.refine_s(flux, psibnd, zero_crossings[0])
        Runa, Zuna, Suna = self.refine_s(flux, psibnd, zero_crossings[-1])       
        if self.plot:
            plt.scatter([Runi,Runa],[Zuni,Zuna])
        return(Runi,Zuni,Suni,Runa,Zuna,Suna)

        


