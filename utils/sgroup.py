# sgroup.py
# SignalGroup class (multiple signals with identical time base)
# 18-Nov-2020  wls  -> python3

import numpy as np
from netCDF4 import Dataset


class SignalGroup:

  def __init__ (self, snames, data, timebase):
    self.snames = snames
    self.data = data
    self.timebase = timebase

  @classmethod
  def from_netcdf(cls, fn):
    f = Dataset(fn, 'r')
#    snames = ((f.variables['snames'])[:]).split(',')
    snames = (f.variables['signalnames'])[:]
    data = (f.variables['data'])[:]
    timebase = (f.variables['timebase'])[:]
    c = cls(snames, data, timebase)
    f.close()
    return c

  def save_netcdf(self, fn, description = ''):
#    str_out = np.array([','.join(self.snames)], dtype='object')
    str_out = np.array([self.snames], dtype='object')
    file = Dataset(fn, 'w', format='NETCDF4')
    file.description = description
    file.createDimension('ntimes', self.data.shape[0])
    file.createDimension('nsignals', self.data.shape[1])
    file.createDimension('strdim', str_out.size)
    nc_data = file.createVariable('data', 'f8', ('ntimes', 'nsignals'))
    nc_timebase = file.createVariable('timebase', 'f8', ('ntimes'))
    nc_snames = file.createVariable('signalnames', str, ('strdim'))
    nc_data[:,:] = self.data
    nc_timebase[:] = self.timebase
    nc_snames[:] = str_out  
    file.close()

# return the first signal that matches a given name
  def by_name(self, sname):
    nset = (np.array(self.snames) == sname)
    for i,b in enumerate(nset):
      if b:
        return self.data[:,i]
#    print('Cannot find signal >'+sname+'<')
    return None


  def by_names(self, snames, replace_with_zeros=False):
    '''
       return signal array for given list of names
       replace_with_zeros: if a signal is not found in sgroup, return an array of zeros
    '''
    sigs = None
    first = True
    fnames = []  # signals found
    for j, sname in enumerate(snames):
      if np.all(np.array(self.snames) != sname):
        if replace_with_zeros:
          print('Cannot find signal "%s" - replace with zeros' % sname)        
          s1 = np.zeros(len(self.timebase))
          if first:
            sigs = s1
            fnames = [sname]
            first = False
          else:
            sigs = np.column_stack((sigs, s1))
            fnames.append(sname)            
        else:
          print('Cannot find signal "%s"' % sname)        
        continue
      nset = (np.array(self.snames) == sname)
      for i,b in enumerate(nset):
        if b:
          if first:
            sigs = self.data[:,i]
            fnames = [sname]
            first = False
          else:
            sigs = np.column_stack((sigs, self.data[:,i]))
            fnames.append(sname)
    return sigs, fnames

