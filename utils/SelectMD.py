def SelectMD(shot):
    path = '/shares/software/aug-dv/moduledata/ads/common/diags/cliste/System_area/xmldata/'
    if shot <= 8045:
        sys.exit("sub select_machine_description_file: no machine description file for DivI shots (# <=8045)")
        
    elif shot <= 14048:
        md_file = path+"AUG_MachineDescription.xml_DivIIa.03"
        print("machine description file for IIa geometry: ",md_file)

    elif shot <= 21485:
        md_file = path+"AUG_MachineDescription.xml_DivIIb.03"
        print("machine description file for IIb geometry: ",md_file)

    elif shot <= 25980:
        md_file = path+"AUG_MachineDescription.xml_DivIIc.03"
        print("machine description file for IIc geometry: ",md_file)

    elif shot <= 30135: # since #25981
        md_file = path+"AUG_MachineDescription.xml_DivIId.03"
        print("machine description file for IId geometry: ",md_file)

    elif shot <= 31399: #! since #30136
        md_file = path+"AUG_MachineDescription.xml_DivIII.381"
        print("machine description file for III geometry: ",md_file)

    elif shot <= 41651: 
        md_file = path+"AUG_MachineDescription.xml_DivIII.400.in_out_btheta_pairs"
        md_file = '/shares/departments/AUG/users/micdu/equil/cliste/sun/MDfiles/AUG_MachineDescription.xml_DivIII.400.one_newpfcoils'
        #!md_file =path+"AUG_MachineDescription.xml_DivIII.400.in_out_btheta_pairs_fl"
    else:
        md_file = '/shares/departments/AUG/users/micdu/equil/cliste/sun/MDfiles/AUG_MachineDescriptionDO_Oct2024.xml'
        #md_file = path + 'MachineDescription_AUG_2024April22.xml'
        print("machine description file for III geometry: ",md_file)

    return md_file
