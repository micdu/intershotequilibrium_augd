import numpy as np
import aug_sfutils
import tempfile,os,shutil
import ctypes as ct

module_path = os.getcwd()


def make_fpsfh(names,diag,fpcof_path,ntimes):
    # ................ create shot file header .................
    # shot file header file name, as dictated by libddww
    sfhname = '%s00000.sfh' % (diag)

    # copy from template into a unique directory we can write to
    td = tempfile.TemporaryDirectory()
    shutil.copyfile(module_path+'/SFH/'+'TMP00000.sfh', td.name+'/'+sfhname)
    old_wd = os.getcwd()
    os.chdir(td.name)

    # ................. edit shot file header ..................
    sfh = aug_sfutils.sfh.SFH()
    err = sfh.Open(sfhname)
    if err!=0:
        print("sfh.Open()  returns", err)

     # rename diagnostic
    diagname = aug_sfutils.str_byt.to_byt(diag)
    c_diag = ct.c_char_p(diagname)
    err = aug_sfutils.sfh.libsfh.sfhrename(sfh.c_sfid, c_diag)
    aug_sfutils.sfh.libsfh.sfherror(err, 'sfhrename')

    description = 'FPG derived from coefficients: %s' %(fpcof_path)
    sfh.set_text(diag, description)
    subtyp = 2  # for floating point types

    # timebase
    tb_name = 'TIMEF'
    #print("Timebase %s, length %d" % (tb_name, ntimes))
    sfh.newobj(tb_name, 8, subtyp)   # timebase
    sfh.Modtim(tb_name, ntimes)

    #remove sname rays, write all others as signals
    names.remove('rays')
    print(names)
    for nam in names:
        #print("Signal %s, length %d" % (k, siglen))
        sfh.newobj(nam, 7, subtyp)   # signal
        sfh.Modtim(nam, ntimes)
        sfh.newrelt(nam, tb_name)

    print('clear')
    sfh.newobj('rays',6,subtyp)
    sfh.Modsgr('rays',[24,ntimes])
    sfh.newrelt('rays', tb_name)

    err = sfh.Close()
    if err!=0:
        print("sfh.Close()  returns", err)

    # copy back sfh for debugging
    shutil.copyfile(td.name+'/'+sfhname, module_path+'/SFH/'+sfhname)
    os.chdir(old_wd)  # back to where we were before
    
    
