'''
  sgroup_augww.py  - write a signal group to an AUG shot file

'''
import sys

# add path to sfaugutils
paths = ['/shares/software/aug-dv/moduledata/aug_sfutils/0.9.1', '/shares/departments/AUG/users/git/soft/pymysql/1.0.2']
for p in paths:
    if not p in sys.path:
        sys.path.append(p)

# absolute path of executed python script
import inspect, os
#module_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
module_path = os.getcwd()
import tempfile
import numpy as np
import shutil     # for copyfile()
import ctypes as ct
import getpass    # for getuser()

import aug_sfutils

# ----------------------------------------------------------------------------------

def sgroup_write_augsf(s, shot, diag, tb_name='time', description=None, \
                       experiment='self', verbose=False):
    '''
      write a signal group 's' (object as defined by 'SignalGroup' class in sgroup.py)
      to an AUG shotfile  (experiment:diag shot)

      tb_name: name of the time base (time base is common for all signals)
    '''

    if experiment=='self':
        experiment = getpass.getuser()    
    # ................ create shot file header .................
    # shot file header file name, as dictated by libddww
    sfhname = '%s00000.sfh' % (diag)

    # copy from template into a unique directory we can write to
    td = tempfile.TemporaryDirectory()
    shutil.copyfile(module_path+'/SFH/'+'TMP00000.sfh', td.name+'/'+sfhname)
    old_wd = os.getcwd()
    os.chdir(td.name)

    # ................. edit shot file header ..................
    sfh = aug_sfutils.sfh.SFH()
    err = sfh.Open(sfhname)
    if err!=0:
        print("sfh.Open()  returns", err)

    # rename diagnostic
    diagname = aug_sfutils.str_byt.to_byt(diag)
    c_diag = ct.c_char_p(diagname)
    err = aug_sfutils.sfh.libsfh.sfhrename(sfh.c_sfid, c_diag)
    aug_sfutils.sfh.libsfh.sfherror(err, 'sfhrename')

    # add description
    if description is not None:
        sfh.set_text(diag, description)

    subtyp = 2  # whatever this means

    # timebase
    ntimes = len(s.timebase)
    #print("Timebase %s, length %d" % (tb_name, ntimes))
    sfh.newobj(tb_name, 8, subtyp)   # timebase
    sfh.Modtim(tb_name, ntimes)

    # signal traces
    siglen = s.data.shape[0]
    for k in s.snames:
        #print("Signal %s, length %d" % (k, siglen))
        sfh.newobj(k, 7, subtyp)   # signal
        sfh.Modtim(k, siglen)
        sfh.newrelt(k, tb_name)

    err = sfh.Close()
    if err!=0:
        print("sfh.Close()  returns", err)

    # copy back sfh for debugging
    shutil.copyfile(td.name+'/'+sfhname, module_path+'/SFH/'+sfhname)
        
    # ................. write data ..................

    # make a dictionary out of the signal group
    d = {tb_name: s.timebase}
    for i, sname in enumerate(s.snames):
        d[sname] = s.data[:, i]

    # write to shot file at once
    aug_sfutils.ww.write_sf(shot, d, module_path+'/SFH', diag, exp=experiment)

    os.chdir(old_wd)  # back to where we were before


# end of sgroup_augww.py
