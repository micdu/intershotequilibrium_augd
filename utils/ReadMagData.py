import numpy as np
import aug_sfutils as dd
import matplotlib.pyplot as plt
import scipy.stats as stats
import re,sys

btfabb_corr = 1.005

class dataset:
    def __init__(self):
    #Define common blocks for all subsets of required data
        self.diag = 'MAY' #where is the data stored
        self.names = []
        self.data = []
        self.times = []
        self.compare = False
        self.offsetcorrect = True
        self.btcorrect = False
        self.toff = np.arange(100)/99.*0.1 - 9.9
        #self.toff = np.arange(100)/99.*0.01 - 3.96
        self.toff1 = -9.9
        self.toff2 = -8.9

        self.tbt_corr1 = -9.1
        self.tbt_corr2 = -5.1
        
    def ReadData(self,shot,bt=None,tbt=None,tin=None,
                 bref=None,tbref=None):
        #toff = -9.9
        #tbt_corr = -4.5
        #try:
        sf = dd.SFREAD(self.diag,shot)
        time = sf.gettimebase(self.names[0])
        whoff = np.logical_and(time>=self.toff1, time<=self.toff2)
        if not np.any(whoff):
            print('Forcing no offset correct (no offset data available)')
            self.offsetcorrect = False
        
        whbt_corr = np.logical_and(time>=self.tbt_corr1, time<=self.tbt_corr2)
        if not np.any(whbt_corr):
            print('Forcing no BT correction (no offset data available)')
            #self.bttcorrect = False
            
        if self.btcorrect:
            bt_data = sf.getobject('BTF')
            
        for sig in self.names:
            try:
                temp = sf.getobject(sig)
            except:
                print('no signal data for: ',sig)
                temp = np.zeros_like(time)
            if (temp is None):
                temp = np.zeros_like(time)
                
            if self.offsetcorrect:
                offset = np.mean(temp[whoff])
                temp = temp - offset
              
            if self.btcorrect:
                try:
                    slope = np.mean(np.interp(tbref,time,temp) / bref)
                    result = stats.linregress(bt_data[whbt_corr],temp[whbt_corr])
                    #print('BT corrections for signal: ',sig)
                    #print(slope,result.slope)
                    correction = result.slope * bt_data
                    temp = temp - correction
                except:
                    temp = temp
            else:
                slope = 1
            #always interpolate onto common time base
            temp = np.interp(tin,time,temp)
            self.data.append(np.array(temp))
            
            #if self.diag == 'FPG':
            #    print(sig)
            #    plt.plot(tin,temp)
            #    plt.show()
            
            if self.compare:
                print('comparing')
                try:
                    plt.plot(tin,temp,label='Mine')
                    sf1 = dd.SFREAD('MAP',shot)
                    t2 = sf1.gettimebase(self.names[0])
                    comp = sf1.getobject(sig)
                    
                    plt.plot(t2,comp,label='MAP')
                     
                    sf2 = dd.SFREAD('DDS',shot)
                    if re.match('^Dp',sig):
                        sig_loc = sig[0]+sig[1].capitalize()+sig[2:-1]
                    else:
                        sig_loc = sig
                    t3 = sf2.gettimebase(sig_loc)
                    comp2 = sf2.getobject(sig_loc)
                    plt.plot(t3,comp2,label='DDS')
                    
                    plt.legend()
                    plt.title(sig)
                    plt.xlim(-0.2,0.2)
                    plt.show()
                except:
                    print('No MAP/DDS data available for this signal')
        #except Exception as e:
        #    print(e)
        #    print("Guess I'll die")
        #    if tin.any:
        #        self.data.append(np.array(tin * 0.))
           
        self.times=tin

def ReadBT(shot,tbref=None):
    #Read BT_NOM from MAY
    if shot > 0000:
        may = dd.SFREAD('MAY',shot)
        btmay = may.getobject('BTF')
        tmay = may.gettimebase('BTF')
        signbtmay = np.sign(np.interp(0.1,tmay,btmay))
        
        #Read accurate BT from MBI
        mbi = dd.SFREAD('MBI',shot)
        bt = mbi.getobject('BTFABB')  * btfabb_corr
        tbt = mbi.gettimebase('BTFABB')
        
        signbt = np.sign(np.interp(0.1,tbt,bt))
        if signbt != signbtmay:
            bt = -1. * bt         
    else:
        mai = dd.SFREAD('MAI',shot)
        bt = mai.getobject('BTF')
        tbt = mai.gettimebase('BTF')

    tbref = np.arange(100)/99. * 1. - 5.5
    print(tbref)
    bref = np.interp(tbref,tbt,bt)   
    return bt,tbt,tbref,bref,np.max(tmay)
