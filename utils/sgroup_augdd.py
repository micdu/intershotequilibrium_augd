#!/usr/bin/env python
# sgroup_mdsaug.py
# SignalGroup subclass to read AUG data with sfaug


import sys

# add path to sfaugutils
#paths = ['/shares/software/aug-dv/moduledata/aug_sfutils/0.9.1', '/shares/departments/AUG/users/git/soft/pymysql/1.0.2']
#for p in paths:
#    if not p in sys.path:
#        sys.path.append(p)


import aug_sfutils

import numpy as np
from utils.sgroup import SignalGroup
import re

class SignalGroup_augdd (SignalGroup):

  def __init__ (self, signals, diagnostic, shot, experiment='AUGD', edition=0,
                changePSL = False):
    '''
    Signal group from a list of signals within one AUG diagnostics

    :signals:      list of strings with signal names to read
    :diagnostic:   diag (shotfile) code 
    :shot:         ASDEX Upgrade shot number
    :experiment:   experiment code, default: AUGD
    :edition:      shot file edition, default: 0
    '''
    signals_loc = signals.copy()
    if diagnostic == 'DDS': #DDS write DPsixx instead of Dpsixx
        print('changing signal names')
        for i,sig in enumerate(signals_loc):
            if re.match('^Dp',sig):
                signals_loc[i] = sig[0]+sig[1].capitalize()+sig[2:-1]

    if changePSL:
        print('Using Ipsl[o,u]k')
        wh = np.where(signals_loc == 'Ipslon')
        signals_loc[wh] = 'Ipslok'
        wh = np.where(signals_loc == 'Ipslun')
        signals_loc[wh] = 'Ipsluk'
                

    sfo = aug_sfutils.SFREAD(shot, diagnostic, experiment=experiment, edition=edition)    
    timebase = sfo.gettimebase(signals_loc[0])   # first signal defines timebase

    snames = []
    data = None
    for i, s in enumerate(signals_loc):
      sig1 = sfo.getobject(s)
      if sig1 is None:
        continue
      if data is None:  # first signal found
        data = sig1
        snames = [signals[i]]
      else:             # append to previous
        data = np.column_stack((data, sig1))
        snames.append(signals[i]) #unpleasant hack to populate names with original names,while reading DDS names
    print("%s:%s(%d) %d, %d signals, %d time points, " % (experiment, diagnostic, edition, \
                                                          shot, len(snames), len(timebase)))

    
    if diagnostic == 'DDS':
        wh = np.where((timebase > -0.5) & (timebase < 10))
        self.data = data[wh,:].squeeze()
        self.timebase = timebase[wh]
    else:
        self.data = data
        self.timebase = timebase

    
        
    self.snames = snames
