import subprocess,sys
import shutil
from CLISTEPrep import CLISTEPrep
from sfaugww import write_augsf_eqrzfd as eqww
from cliste_eqdsk import cliste_eqdsk

def EQH_workflow(shot=33173,t1=2.,t2=3.,tdist=0.001,
                 InTime=None,diaW='EQI',expW='AUGD'):
    
    print("Running CLISTE for shot "+str(shot)+" in time window " + str(t1)+"-"+str(t2))
    rootdir, md_file = CLISTEPrep(shot=shot,t1=t1,t2=t2,tdist=tdist,
                                  batchmode=True,write=True,diag=diaW)
    
    print("Running all timepoints now")
    subprocess.run(["./exec_cliste.pl","-tmpdir",rootdir,"-mdfile",md_file])
    try:
        eq = cliste_eqdsk(rootdir+'/eqdsk',fn_eqres=rootdir+'/result')
        eqww(shot,diaW,eq,experiment=expW)
        shutil.rmtree(rootdir)
    except:
        print('An error occurred in writing EQI')
    
    
def parseargs():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('shot',type=int)
    parser.add_argument('-t1',type=float,default=0.)
    parser.add_argument('-t2',type=float,default=10.)
    parser.add_argument('-tdist',type=float,default=0.025)
    parser.add_argument('-InTime',type=str,default=None)
    parser.add_argument('-diaW',type=str,default='EQI')
    parser.add_argument('-expW',type=str,default='AUGD')
    return parser.parse_args()


if __name__ == '__main__':
    args = parseargs()
    EQH_workflow(args.shot,args.t1,args.t2,tdist=args.tdist,
                 InTime=args.InTime,expW=args.expW,diaW=args.diaW)
