import tempfile,os,subprocess,shutil,glob
from utils import SelectMD as FMD
from cliste_eqres import cliste_eqres

#Define global directories
root = '/shares/departments/AUG/users/'+os.getenv('USER')+'/equil/cliste/'
datadir = root+'data/'
resdir = root+'results/'
executable = root+'cliste_aug/lib_citrix/cliste.out'

endinv = '/ENDINV ========================================================================\n'
eof = ' /EOF --------------------------------------------------------------------------\n'


def RunCliste(infile,mdfile,
              flagstxt,fmp_text,
              xtr_text,
              executable):
    tmp_dir = tempfile.mkdtemp(prefix='CLISTE')+'/'
    print(tmp_dir)
    
    empty_files = ['fort.18','fort.19','fort.20','fort.21','fort.22','fort.23','fort.24','fort.26','fort.32']
    for file in empty_files:
        f=open(tmp_dir+file,'w')
        f.write('0')
        f.close()
    with open(tmp_dir+'fort.15','w') as f:
        f.write(flagstxt+fmp_text)
    with open(tmp_dir+'fort.31','w') as f:
        f.write(xtr_text)
    shutil.copy(infile,tmp_dir+'fort.13')
    shutil.copy(mdfile,tmp_dir+'MDfile.xml')
    
    
    wd = os.getcwd()
    os.chdir(tmp_dir)
    process = subprocess.run(executable)
    os.chdir(wd)

    return tmp_dir
    
def runcl(shot,ind1,ind2,step,debug=False):

    shot_str = str(shot)
    cmp_str = shot_str[0:2]
    print(shot_str,cmp_str)

    #Define directories
    data_loc = datadir+cmp_str+'/'+shot_str+'/'+shot_str
    tmp = resdir+cmp_str+'/'
    if not os.path.isdir(tmp):
        os.mkdir(tmp)
    savedir = resdir+cmp_str+'/'+shot_str+'/'
    if not os.path.isdir(savedir):
        os.mkdir(savedir)
        
    if os.path.isfile(savedir+'count'):
        with open(savedir+'count') as f:
            run=f'{int(f.read())+1:04}'
    else:
        run='0001'
    with open(savedir+'count','w') as f:
        f.write(run)

    #Define data + settings files, including defaults
    mdfile = FMD.SelectMD(shot)
    
    infile = data_loc+'.cliste.in'
    if not os.path.isfile(infile):
        infile = datadir+'AUGcliste.in.eqh'

    flagsfile = data_loc+'.flags'
    if not os.path.isfile(flagsfile):
        flagsfile = datadir+'DivIII.flags.kin'

    fmpfile = data_loc+'.fmp'
    xtrfile = data_loc+'.xtr'
    xtrmfile = data_loc+'.xtrm'

    #set up data 
    with open(flagsfile) as f:
        flagstext = f.read()
        
    with open(fmpfile) as f:
        fmptext = f.read()
    fmp_header = fmptext.split(endinv)[0]
    fmp_data = fmptext.split(endinv)[1].split(eof)

    xtr_txt='0' #Define default text for xtr file
    if os.path.isfile(xtrfile):
        with open(xtrfile) as f:
            xtr_txt = f.read()
            xtr_txt = '1\n'+xtr_txt
            
    #Loop through all selected indices
    for ind in range(ind1-1,ind2,step):
        print(ind)
        if os.path.isfile(xtrmfile):
            with open(xtrmfile) as f:
                xtrtext=f.read()
            xtr_header = xtrtext.split(endinv)[0]
            xtr_data = xtrtext.split(endinv)[1].split(eof)
            xtr_txt = '1\n'+xtr_header + xtr_data[ind]

    
        rundir = RunCliste(infile,mdfile,
                           flagstext,fmp_header+fmp_data[ind],
                           xtr_txt,
                           executable)

        t_int = (int(float(fmp_data[ind].split('\n')[0].split('t=')[1][0:7])*1e5))
        timestamp = f'{t_int:07}'
        if os.path.isfile(rundir+'fort.8'):
            shutil.copy(rundir+'fort.8',savedir+shot_str
                        +'.eqres.'+run+'.'+timestamp+'s')
        
        if os.path.isfile(rundir+'fort.56'):
            shutil.copy(rundir+'fort.56',savedir+shot_str
                        +'.eqdsk.'+run+'.'+timestamp+'s')
            
        if os.path.isfile(rundir+'fort.97'):
            shutil.copy(rundir+'fort.97',savedir+shot_str
                        +'.eqinp.'+run+'.'+timestamp+'s')

        if debug:
            if os.path.isfile(rundir+'cliste.log'):
                shutil.copy(rundir+'cliste.log',savedir+shot_str
                        +'.eqout.'+run+'.'+timestamp+'s')
        shutil.rmtree(rundir)
        
    list_dsk = glob.glob(savedir+shot_str+'.eqdsk.'+run+'*')
    with open(savedir+'result','w') as wres:
        with open(infile) as rf:
            shutil.copyfileobj(rf,wres)
        wres.write('N_EQUIL: '+str(len(list_dsk))+'\n')
                
        with open(savedir+'eqdsk','w') as wf: #save only eqres files with eqdsk
            for file in list_dsk:
                with open(file) as rf:
                    shutil.copyfileobj(rf,wf)
                f_res = file.replace('eqdsk','eqres')
                with open(f_res) as rf:
                    shutil.copyfileobj(rf,wres)

    cliste_eqres(savedir+'result')

                    
def parseargs():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('shot',type=int)
    parser.add_argument('ind1',type=int,default=1)
    parser.add_argument('ind2',type=int,default=1)
    parser.add_argument('step',type=int,default=1)
    return parser.parse_args()

if __name__ == '__main__':
    args=parseargs()
    runcl(args.shot,args.ind1,args.ind2,args.step)






    
