import numpy as np
import sys
import subprocess as sp

shots,times1,times2 = np.loadtxt('NEW_TIME_WINDOW_final.txt',skiprows=1,unpack=True)
print(shots,times1,times2)

for i,shot in enumerate(shots):
    print(shot,times1[i],times2[i])
    sn = int(shot)
    t1 = times1[i]
    t2 = times2[i]
    sp.run(["python","EQH_workflow.py","%d"%sn,"-tdist","0.0001",
            "-expW","AUGE","-diaW","EQH",
            "-t1","%f"%t1,"-t2","%f"%t2])

