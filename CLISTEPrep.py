import sys
#sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
#import dd
import numpy as np
import matplotlib.pyplot as plt
import os

import aug_sfutils as dd
from utils import SelectMD as FMD
from utils.ReadMagData import dataset,ReadBT
Plot = False
btfabb_corr = 1.005


def writeData(filehandle,data,inform=None):
    for i in range(int(np.floor(len(data)/10))+1):
        string=''
        for entry in data[i*10:(i+1)*10]:
            try:
                if inform is not None:
                    string = string + inform.format(entry) 
                else:
                    string = string + '{:13.5e}'.format(entry)
            except:
                print(entry)
        filehandle.write(string+'\n')

    
def CLISTEPrep(shot=33173,t1=2.,t2=3.,tdist=0.001,
               batchmode=False,outdir=False,write=True,
               ShowPlots=False,InTime=None,diag='EQH'):
    
    import xml.etree.ElementTree as ET
    md_file = FMD.SelectMD(shot)
    tree = ET.parse(md_file)
    root = tree.getroot()

    fpc = dd.SFREAD('FPC',shot)
    ip = np.array(fpc.getobject('IpiFP'))
    iptime = fpc.gettimebase('IpiFP')


    print('Old t1,t2:')
    print(t1,t2)
    #only take times where ip gt 10 kA:
    wh = np.where(np.abs(ip) > 0.01e6)
    
    t1 = np.max([t1,iptime[wh][0]])
    t2 = np.min([t2,iptime[wh][-1]])
    print('New t1,t2: ')
    print(t1,t2)
    sgn_ip = np.sign(np.interp(0.1,iptime,ip))
    #make minimum Ip 0.3 MA to help convergence
    ip[(np.abs(ip) < 0.1e6)] = 0.1e6 * sgn_ip

    if InTime:
        time=np.loadtxt(InTime,usecols=0,skiprows=1)
    else:
        #create timebase for later use
        ntimes = (t2 - t1 ) / tdist + 1
        #time = np.linspace(t1,t2,num=ntimes)
        time = np.arange(ntimes) / (ntimes - 1.) * (t2 - t1) + t1

    fpg = dataset()
    bpol = dataset()
    dpsi = dataset()
    pfcoils = dataset()
    psl = dataset()
    ipolsol = dataset()
    
    fpg.diag = 'FPG'
    fpg.names = ['Rsquad',  'Zsquad',  'Rgeo',    'Zgeo',    'Rxpu',    'Zxpu',    'ahor',    'k',  'Rin',     'Raus',
                 'Zunt',    'Rzunt',   'Zoben',   'Rzoben',  'Rmag',    'Zmag',     'Vol', 'IVSF','bpli2',   'betpol',
                 'li',      'Wmhd',    'Circumf', 'bver',  'q95/q0', 'Finstab',   'Fstab',     'Rax-Rgeo',  'fax-bnd',
                 'q0',      'q95',        'q25',   'q50',     'q75',   'Zgeri',   'Zgera',       'Zskewi',   'Zskewa',
                 'Rroofi',  'Rroofa', 'lenH-4','delRoben','delRuntn','GapAbot', 'GapAmid',      'GapAtop',  'GapAmin',
                 'lenH-5',  'lenV-1',  'koben',  'kunten',  'lenV-2', 'lenV-3',    'dRXP',     'Dia_Flux',   'Sogeri',
                 'Sogera',  'Rogeri',  'Rogera',  'roUIoo',  'roUIooou','roUIouIm','roUImIu', 'roUIuMi',   'roUMiMoi',
                 'roUMoioa','roUMoaMa','roUMaAu', 'roUAuAm', 'roUAmAo', 'PSIBD', 
                 'ray___15','ray___30','ray___45','ray___60','ray___75','ray___90','ray__105',
                 'ray__120','ray__135','ray__150','ray__165','ray__180','ray__195','ray__210', 
                 'ray__225','ray__240','ray__255','ray__270','ray__285','ray__300','ray__315',
                 'ray__330','ray__345','ray__000']
    fpg.offsetcorrect = False
    nfpg = len(fpg.names)
    check='ray'
    nray = len([idx for idx in fpg.names if idx.lower().startswith(check.lower())])
    nfplst = nfpg - nray
    whrmag = fpg.names.index('Rmag')
    whbeta = fpg.names.index('betpol')

    bt,tbt,tbref,bref,tmay_max = ReadBT(shot)
    
    #Read Bpol probe data
    bpol.names = root.findall("./magdiag/bpol_probes/setup_bprobe/name")[0].text.replace('\n',"").replace("'","").replace(" ","").split(',')
    bpol.btcorrect = True
    #bpol.compare = True
    bpol.ReadData(shot,bt=bt,tbt=tbt,tin=time,bref=bref,tbref=tbref)
    bpol.data = np.array(bpol.data)
    nbpol = np.min([len(bpol.names),62])
    
    #Read flux loop data
    dpsi.names = root.findall("./magdiag/flux_loops/setup_floops/name")[0].text.replace('\n',"").replace("'","").replace(" ","").split(',')
    dpsi.btcorrect = True
    #dpsi.compare = True
    dpsi.ReadData(shot,bt=bt,tbt=tbt,tin=time,bref=bref,tbref=tbref)
    dpsi.data = np.array(dpsi.data)
    ndpsi = len(dpsi.names)
    
    #Read PF coil data
    pfcoils.names = ['IV1o','IV1u','IV2o','IV2u','IV3o','IV3u',
                     'Ipsluk','Ipslok','ICoIo','ICoIu','IOH','Doi','Doa',
                     'dIOH2s','dIOH2u']
    #pfcoils.toff = np.arange(100)/99. * 0.1 - 0.75
    pfcoils.ReadData(shot,tin=time)
    pfcoils.data=np.array(pfcoils.data)

    psl.names = ['Ipsluk','Ipslok']
    psl.toff = np.arange(100)/99. * 0.1 - 0.5
    psl.ReadData(shot,tin=time)
    psl.data = np.array(psl.data)
    if ShowPlots:
        plt.plot(time,psl.data[0,:])
        plt.plot(time,psl.data[1,:])
        plt.show()
    
    whpsl = [i for i,s in enumerate(pfcoils.names) if 'psl' in s]
    pfcoils.data[whpsl,:] = psl.data
    
    #Read Ipolsoli,a
    ipolsol.diag='MAC'
    ipolsol.names = ['Ipolsola','Ipolsoli']
    try:
        ipolsol.ReadData(shot,tin=time)
        ipolsol.data=np.array(ipolsol.data)
    except:
        ipolsol.data=np.zeros((2,time.size))
    
    #Read FPG
    fpg.ReadData(shot,t1,t2,tin=time)
    fpg.data=np.array(fpg.data)
    wh = fpg.names.index('Wmhd')
    fpg.data[wh,:] = fpg.data[wh,:]/1e6 #correct Wmhd to be in MJ
    wh = np.where(np.abs(fpg.data) > 90)
    if np.any(wh):
        fpg.data[wh] = 1.
    wh = np.where(fpg.data[whrmag,:] > 1.9)
    if np.any(wh):
        fpg.data[whrmag,wh] = 1.65
    wh = np.where(fpg.data[whbeta,:] < 0.)
    if np.any(wh):
        fpg.data[whbeta,wh] = 0.5
        fpg.data[whbeta-1,wh] = 1 #bpli2
    
    #make bt onto input time range
    bt=np.interp(time,tbt,bt)
    ip=np.interp(time,iptime,ip)/1e6
    #ip=np.interp(time,tmay,ip)
    #all data sets now have same time basis - proceed to write...
    str_shot = str(shot)
    campaign = str_shot[0:2]

    if batchmode:
        rootdir = '/tmp/'+diag+'_new.'+os.getenv('USER')+'.'+str_shot
        filename = rootdir + '/fort.17'
        if not os.path.isdir(rootdir):
            os.mkdir(rootdir)    
    elif outdir:
        rootdir = outdir
        filename = rootdir+str_shot+'.fmp'
    else:
        rootdir = '/shares/departments/AUG/users/'+os.getenv('USER')+'/equil/cliste/data/'
        if not os.path.isdir(rootdir+campaign+'/'):
            os.mkdir(rootdir+campaign+'/')
        if not os.path.isdir(rootdir+campaign+'/'+str_shot+'/'):
            os.mkdir(rootdir+campaign+'/'+str_shot+'/')
        filename = rootdir+campaign+'/'+str_shot+'/'+str_shot+'.fmp'
    

            
    if write:
        #choose how many bpol probes to take:
        #nbpol = 62
        fmp = open(filename,'w')
        fmp.write("augd: FPG_{:5}.001                             sxaug26 __micdu2020-02-06  11:33\n".format(shot))
        fmp.write("{:4}{:4}{:4} nsig=nft+nfi2+2, nft, nfi2\n".format(ndpsi + nbpol +2, ndpsi,nbpol))
        fmp.write("   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000\n")
        fmp.write("   1.000   1.000   1.000   1.000   1.000   1.000\n")
        fmp.write("   5   2    FPvers, sWIX         rPCL2C4   typeTB= 2   dtAv= +-   0 [ms]\n")
        fmp.write("  71  24    iFPlst,Nrays\n")
        fmp.write("/ENDINV ========================================================================\n")
        blockend = " /EOF --------------------------------------------------------------------------\n"
    
            
        for i in range(time.size):
            fmp.write("augd  FPG {:5} 001    t= {:6.4f} [s] <-- {:6.4f} [s]     {} <-- fClFMP\n".format(shot,time[i],time[i],i))
            fmp.write(" {:5} {:6.3f} IpFp{:8.5f} B0 {:7.4f} fpver  5 oh2fl  0 e|p 0 k 4\n".format(shot,time[i],ip[i],bt[i]))
            
            data = dpsi.data[:,i]*1e3 #convert to mT
            writeData(fmp,data)        
            data = np.concatenate((bpol.data[0:nbpol,i]*1e3,ipolsol.data[:,i]/1e3)) 
            writeData(fmp,data)
            data = pfcoils.data[:,i]
            writeData(fmp,data)
            data = fpg.data[:,i]
            writeData(fmp,data,inform='{:8.4f}')
            fmp.write(blockend)

        fmp.close()
        return rootdir, md_file
    else:
        signals={'time':time,'ip':ip,'bt':bt,'dpsi':dpsi.data,'bpol':bpol.data,'ipolsol':ipolsol.data,'pfcoils':pfcoils.data,'fpg':fpg.data}
        return signals

def parseargs():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('shot',type=int)
    parser.add_argument('-t1',type=float,default=0.)
    parser.add_argument('-t2',type=float,default=10.)
    parser.add_argument('-tdist',type=float,default=0.001)
    parser.add_argument('-batchmode',type=str,default=None)
    parser.add_argument('-outdir',type=str,default=None)
    parser.add_argument('-InTime',type=str,default=None)
    return parser.parse_args()

    
if __name__ == "__main__":
    args=parseargs()
    
    CLISTEPrep(args.shot,args.t1,args.t2,tdist=args.tdist,
               batchmode=args.batchmode,outdir=args.outdir,
               InTime=args.InTime)
