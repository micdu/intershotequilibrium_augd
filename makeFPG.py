#!/usr/bin/env python
#  
# FPG.py   - evaluate FPC and FPG for a testshot and
#            plot comparison with reference offline data
# 19-Jan-2016  wls
# 18-Nov-2020  wls  python3
# 06-Mar-2024  wls  read/write data from/to AUG shot files
# 08.10.2024 micdu adapted to run inter-shot FPC/FPG
import sys
import numpy as np
import scipy.signal   # decimate()
import matplotlib.pyplot as plt

from utils.sgroup import SignalGroup
from utils.fpcof import FPcoefficients
from utils.make_fpsfh import make_fpsfh
import utils.sgroup_augdd as sgroup_augdd
import pdb
#from IPython import embed
import aug_sfutils

FPGnam = np.array(['Rsquad','Zsquad','Rgeo','Zgeo','Rxpu','Zxpu','ahor','k','Rin','Raus','Zunt','Rzunt','Zoben','Rzoben','Rmag','Zmag','Vol','IVSF','Slunt','Srunt','fXP1fIL','fXP1fAL','XPfdif','lenH-0','lenH-1','lenH-2','Rxpo','Zxpo','Slobn','Srobn','bpli2','betpol','li','Wmhd','Circumf','bver','q95/q0','Finstab','Fstab','Rax-Rgeo','fax-bnd','fbnd-ref','q0','q95','fbnd-f12','fbnd-f17','fbnd-f22','fbnd-f25','q25','q50','q75','lenH-4','delRoben','delRuntn','GapAbot','GapAtop','GapAmin','lenH-5','lenV-1','koben','kuntn','lenV-2','lenV-3','dRXP','Zgeri2b','Zgera2b','Zskewi2b','Zskewa2b','Zgera','Zgeri','Sloben','Sroben','Rroofi2b','Rroofa2b'])

RAYnam = np.array(['ray___15','ray___30','ray___45','ray___60','ray___75','ray___90',
                   'ray__105','ray__120','ray__135','ray__150','ray__165','ray__180',
                   'ray__195','ray__210','ray__225','ray__240','ray__255','ray__270',
                   'ray__285','ray__300','ray__315','ray__330','ray__345','ray__000'])
angles = np.array([15,30,45,60,75,90,105,120,135,150,165,180,195,210,225,240,255,270,285,300,315,330,345,0])/180*np.pi
print(angles)
# ------------------------- default settings  ----------------------
# path to netCDF file with default FP coefficients
#fpcof_path = '/shares/departments/AUG/users/wls/AUG/FP_coefficients/current.nc'
fpcof_path = '/shares/departments/AUG/users/micdu/Python/FPregression/FP_coefficients/fpcof_2024A_md2024Dec01_nodpsi02.nc'
# verbosity
verbose = True


# Magnetics input data
read_augsf = True     # read from AUG shot file
MAG_experiment = 'AUGD'
MAG_diag = 'MAP'
MAG_edition = 0


# if True, take DivIIo currents into account for plasma current reconstruction
with_DivIIo = False

# time base decimation to reduce computational effort
decimation_factor = 1
# widen (by a factor) vmin-vmax window for acceptable probe readings (1.0 = no widening)
vwide = 10.0
# minimum acceptable number of probes that are in confidence range
min_ok = 62
# option to replace up to (nprobes-min_ok) faulty sensors with their database mean values
replace_with_vmean = False
# choose whether to re-scale Wmhd, q95 etc for actial Ip an dBt (yes!)
post_process_fpg = True
subtract_vacfield = None
replace_probe = None

# write to AUG shot file
write_augsf = True          # write to AUG shot file
FPG_shot = None             # = same as input shot
FPG_experiment = 'self'     # will be replaced by user id
FPG_diag = 'FPG'


def makeFPG(shot,MAG_diag='MAP',MAG_experiment='AUGD',MAG_edition=0,
            FPG_diag='FPG',FPG_experiment='self',
            fpcof_path=fpcof_path,verbose=False,write_augsf=True):
  
  if verbose:
    print("==================================================================================================")
    print('FPG.py')
    if read_augsf:
      print ("Read magnetics from AUG shot file %s:%s %d (%d)" % (MAG_experiment, MAG_diag, shot, MAG_edition))
    else:
      print ("Read magnetics from netcdf file %s" % (nc_input_fn))
    if write_augsf:
      print ("Write FPG data to AUG shot file %s:%s %d" % (FPG_experiment, FPG_diag, FPG_shot))

    print("Shot number: %d" % shot)
    print("Use FP coefficients from: %s" % fpcof_path)
    print("==================================================================================================")
    

  # ---------------------- read FP coefficients -----------------------------------
  fp = FPcoefficients.from_netcdf(fpcof_path)
  
  # probe signals / PF currents needed for this set of coefficients
  probenames_in = np.union1d(np.union1d (fp.FPCprobenames, fp.ERSprobenames), \
                             np.union1d (fp.IPprobenames, fp.FPprobenames) )

  # for 'real' plasma shots, PF currents are stored along with magnetic measurements
  if subtract_vacfield is not None:
    probenames_in = np.union1d(probenames_in, subtract_vacfield)
  if with_DivIIo:
    probenames_in = np.union1d(probenames_in, ['IDoDelta'])

  # MAP, MAY files should have a BTF signal
  probenames_in = np.union1d(probenames_in, ['BTF'])

  print('Using following probes for FP evaluation:')
  print(probenames_in)
  print(fp.IPprobenames)

        

  # --------------------- read input (magnetics) data -----------------------------
  magsig = sgroup_augdd.SignalGroup_augdd(probenames_in, MAG_diag, shot, \
                                          experiment=MAG_experiment, edition=MAG_edition,
                                          changePSL=True)    

  # ------- subtract vacuum fields of PF currents from magnetic measurements ---------
  if subtract_vacfield is not None:
    import Streugewichte     # mutual inductances
    # the subset of magnetic probe names  (flux loop differentials or B field probes)
    magprobes = [n for n in magsig.snames if 'Dpsi' in n or 'dpsi' in n or (n[0]=='B' and not 'BTF' in n)]
    imagprobes = np.array([ magsig.snames.index(n) for n in magprobes ])  
    # indices of magnetic probe names in MutualInductance matrix
    isens = np.array([ Streugewichte.SensorNames.index(n) for n in magprobes ])
    # indices of PF currents to subtract vacfield from
    icircuits = np.array([ Streugewichte.PFcoilNames.index(n) for n in subtract_vacfield ])
    MutInd = (Streugewichte.MutualInductances[:, isens])[icircuits,:]
    PFcurrs, PFcurrnames = pfcsig.by_names(subtract_vacfield)
    magsig.data[:, imagprobes] -= np.tensordot(PFcurrs, MutInd, axes=(1,0))

  # ------ input signals for evaluation by fpcof.py routines -------
  INPUT = magsig.data
  INPUT_snames = magsig.snames

  # ------------ PSL currents ------------------------
  PSLnames = ['Ipslon', 'Ipslun']
  PSLsig, PSLnames = magsig.by_names(PSLnames)
  if PSLsig is None:
    print("No PSL currents found in input data - assume zero PSL current.")
    Ipsl_raw = np.zeros((magsig.data.shape[0], len(PSLnames)))
    Ipsl_tot = np.zeros(magsig.data.shape[0])
    INPUT = np.hstack( (INPUT, Ipsl_raw) )
    INPUT_snames.extend(PSLnames)
  else:
    Ipsl_raw = PSLsig    # in A
    Ipsl_tot = np.reshape (Ipsl_raw[:,0] + Ipsl_raw[:,1], (magsig.timebase.size))
  
  # --------------- toroidal field -------------------
  BTF = magsig.by_name('BTF')   # to postprocess safety factor
  if BTF is None:
    print("No BTF data avilable. Assume Bt = 2.5T.")
    BTF = 2.5 * np.ones(Ipsl_tot.shape[0])
    INPUT = np.hstack( (INPUT, np.reshape(BTF, (INPUT.shape[0], 1))) )
    INPUT_snames.append('BTF')
  else:
    BTF = scipy.signal.savgol_filter(BTF, 51, 3)

  # ------------- upper divertor currents --------------
  IDoia_names_in = ['IDoi', 'IDoa']
  IDoia, IDoia_names = magsig.by_names(IDoia_names_in)
  IDo_names_in = ['IDo', 'IDoDelta']
  IDo, IDo_names = magsig.by_names(IDo_names_in)

  if IDoia is None:
    print("No IDoi,a signals available", end='')
    if IDo is None:  # have no upper divertor currents, assume they are zero
      print(" and no No IDoi* signals. Assume no upper divertor coils active.")
      IDoia = np.zeros((magsig.data.shape[0], len(IDoia_names_in)))
    else:
      print("- calculate from IDo, IDoDelta.")
      # IDoa = IDo
      IDoi = -IDo[:,0] -IDo[:,1]   # = -IDo-IDoDelta
      IDoia = np.vstack( (IDoi, IDo_sig[:,0]) ).T
    INPUT = np.hstack( (INPUT, IDoia) )
    INPUT_snames.extend(IDoia_names_in)
  
  if IDo is None:
    print("No IDo, IDoDelta signals available.")
    IDoDelta = -IDoia[:,0] -IDoia[:,1]   # = -IDoi-IDoa
    IDo = np.vstack( (IDoia[:,1], IDoDelta) ).T
    INPUT = np.hstack( (INPUT, IDo) )
    INPUT_snames.extend(IDo_names_in)
      
  INPUT_snames = np.array(INPUT_snames)
  
  # ---------------- IpiFP --------------------------
  IpiFP0, IP_noise = fp.IP_evaluation(INPUT, INPUT_snames)
  IpiFP0 = np.array(IpiFP0)[:,0]
  
  # ---- normalise measurements to Ip in MA ---------
  fpsig_raw, FPnames = magsig.by_names(fp.FPprobenames)
  if len(FPnames) < len(fp.FPprobenames):
    print("Missing FP probes in input data - cannot use this set of FP coefficients.")
    sys.exit()

  INPUT_norm = 1e6 * INPUT / np.outer(IpiFP0, np.ones((1,INPUT.shape[1])))
  fpsig_norm = 1e6 * fpsig_raw / np.outer(IpiFP0, np.ones((1,fpsig_raw.shape[1])))
  Ipsl_norm = 1e6 * Ipsl_raw / np.outer(IpiFP0, np.ones((1,Ipsl_raw.shape[1])))
  
  # ---- widen probe value acceptance window - as done in real-time FPG
  vmax = np.array( fp.vmax + (fp.vmax-fp.vmean)*(vwide-1.0) )
  vmin = np.array( fp.vmin + (fp.vmin-fp.vmean)*(vwide-1.0) )

  # ------ faulty signal detection ------------------
  fpsig_ok = np.logical_and((fpsig_norm >= vmin), (fpsig_norm <= vmax))

  all_ok = np.all(fpsig_ok, axis=1)  # time pts at which all signals are OK
  if np.any(all_ok):
    imin = np.argmax(all_ok)
    imax = magsig.timebase.size - np.argmax(all_ok[-1:0:-1])-1
    print ('All probes ok from t=', magsig.timebase[imin], ' s to ', magsig.timebase[imax], ' s')

  n_ok = np.sum(fpsig_ok, axis=1)    # number of good probes for each time point
  min_ok = np.max(n_ok)-5
  wh_ok = (n_ok >= min_ok)           # time points with acceptable number of probes
  if np.any(wh_ok):
    imin = np.argmax(wh_ok)
    imax = magsig.timebase.size - np.argmax(wh_ok[-1:0:-1])-1
    print (min_ok, ' probes ok from t=', magsig.timebase[imin], ' s to ', magsig.timebase[imax], ' s')
  else:
    print('no minimum number of probes ok')

 
  if replace_with_vmean:
    not_ok = np.logical_not(fpsig_ok)
    fpsig_norm[not_ok] = (np.outer(np.ones([not_ok.shape[0], 1]), fp.vmean))[not_ok]

  # from now on use only time points where a sufficient number of
  # probes can be assumed to be good
  time = np.squeeze(magsig.timebase[wh_ok])
  fpsig = np.mat( fpsig_norm[wh_ok, :] )
  Ipsl = np.mat( Ipsl_norm[wh_ok, :] )   # PSL currents normalised to Ip
  IpiFP = np.array( IpiFP0 )[wh_ok]
  BTF = np.array( BTF )[wh_ok]
  

  IDoia = IDoia[wh_ok,:]
  IDo = IDo[wh_ok,:]
  INPUT = INPUT[wh_ok,:]
  INPUT_norm = INPUT_norm[wh_ok,:]
  
  # -------- sensor replacement --------------------
  if replace_probe is not None:
    repl, repl_noise = fp.ERS_evaluation(INPUT_norm, INPUT_snames)
    whr = (INPUT_snames == replace_probe)
    if np.any(whr):
      print ("Replacing sensor ", replace_probe)
      INPUT_norm[:,whr] = repl[:,whr]
    else:
      print ("Replaced sensor ", replace_probe, ' not found')

  # --------- FPC recovery --------------------------
  if False:   # old method
    nprob = len(fp.FPprobenames)
    fpcquants = fp.fpccof[0,:] + fpsig*fp.fpccof[1:nprob+1,:] \
      + Ipsl*fp.fpccof[nprob+1:nprob+3,:] + IDoia*fp.fpccof[nprob+3:nprob+5,:]
    # for some FPC quantities, the square is regressed, so need to take square root
    wh = np.where( (fp.FPCnames=='Rslin') | (fp.FPCnames=='Rsqlin') | (fp.FPCnames=='Rs2') ) [0]
    fpcquants[:,wh] = np.sqrt(fpcquants[:,wh])
  else:
    fpcquants, FPCnoise = fp.FPC_evaluation(INPUT_norm, INPUT_snames)

  # ---------------  limiter fluxes -----------------
  iflim, oflim, iflim_var, oflim_var = fp.LIM_evaluation(INPUT_norm, INPUT_snames)
  iflim_max=np.amax(iflim, axis=1)    # maximum flux inner limiter
  oflim_max=np.amax(oflim, axis=1)    # maximum flux outer limiter

  # --------- eigenvector projection ----------------
  psiXu, psiXo, XUNoiseMean, XONoiseMean = fp.Xpt_evaluation(INPUT_norm, INPUT_snames)

  # ----------- plasma boundary category ------------
  psi_bdrycand = np.column_stack( (iflim_max, oflim_max, psiXo, psiXu) )  # needed later
  ikat = fp.category_evaluation(INPUT_norm, INPUT_snames)

  # ---------------- FPG recovery -------------------
  fpgquants, FPGNoiseMean = fp.FPG_evaluation(INPUT_norm, INPUT_snames, ikat)
  if post_process_fpg:
    wh = np.where((fp.FPGnames == 'Wmhd') | (fp.FPGnames == 'Fstab') | (fp.FPGnames == 'Finstab'))[0]
    if len(wh)>0:
      corrfact = 1e-12 * np.square(IpiFP)
      for wh1 in wh:
        fpgquants[:,wh1] = np.array(fpgquants)[:,wh1] * corrfact

    wh = np.where((fp.FPGnames == 'fbnd-f12') | (fp.FPGnames == 'fbnd-fref') | (fp.FPGnames == 'fax-bnd') )[0]
    if len(wh) > 0:
      corrfact = 1e-6 * IpiFP.squeeze()
      for wh1 in wh:
        fpgquants[:,wh1] = np.array(fpgquants)[:,wh1] * corrfact

    wh = np.where((fp.FPGnames=='q0') | (fp.FPGnames=='q25') \
      | (fp.FPGnames=='q50')| (fp.FPGnames=='q75') | (fp.FPGnames=='q95') )[0]
    if len(wh)>0:
      if BTF is None:
        pass
      corrfact = 1e6 * np.divide(BTF, IpiFP) / 2.5
      for wh1 in wh:
        fpgquants[:,wh1] = np.array(fpgquants)[:,wh1] * corrfact

  # --------------------- write to file -----------------------
  if write_augsf:
    # build sgroup structure from output data
    snames = ['IpiFP']
    data =  np.expand_dims(IpiFP, axis=1)
  
    if BTF is not None:
      snames.append('BTF')
      data = np.hstack((data, np.expand_dims(BTF, axis=1) ))
      
    # FPC and limiter/x-point fluxes
    FPCnames = [k[0:8] for k in list(fp.FPCnames)]
    snames.extend (FPCnames + ['FILIMAX', 'FALIMAX', 'PSIXO', 'PSIXU', 'ikCAT'])
    data = np.column_stack(( data, fpcquants, psi_bdrycand, np.float64(ikat) ))
    FPC = SignalGroup(snames,data,time)
    if write_augsf:
      from utils.sgroup_augww import sgroup_write_augsf
      sgroup_write_augsf (FPC, shot, 'FPC', tb_name='TIMEF', \
                          experiment=FPG_experiment, verbose=verbose,
                          description='FPC from wls FPcoefficients')

    #make FPG dictionary, also including FPC types
    FPGdict = {}
    for i,nam in enumerate(snames):
      FPGdict[nam] = data[:,i]
      
    # FPG
    snames.extend (list(fp.FPGnames))
    data = np.column_stack(( data, fpgquants ))
    FPG = SignalGroup(snames, data, time)
    try:
      wh = snames.index('kunten')
      snames[wh] = 'kuntn'
    except:
      print('Could not rename kunten')

    #Build FP dictionary to store as SF
    
    for nam in FPGnam:
      try:
        wh = np.where(fp.FPGnames == nam)
        dat = fpgquants[:,wh].squeeze()
        sh = dat.shape
        if len(sh) > 1:
          print(nam)
          dat = np.squeeze(np.zeros_like(time))
          
        FPGdict[nam] = dat
      except:
        print(nam,' doesnt work')
        FPGdict[nam] = np.squeeze(np.zeros_like(time))
        

        
    rays = []
    for nam in RAYnam:
      try:
        wh = np.where(fp.FPGnames == nam)
        dat = np.squeeze(fpgquants[:,wh])
        rays.append(dat)
        FPGdict[nam] = dat
      except:
        rays.append(time * 0.)
        FPGdict[nam] = np.squeeze(np.zeros_like(time))
        
    FPGdict['rays'] = np.transpose(np.asarray(rays))
    FPGdict['TIMEF'] = time
    FPGdict['ikCAT'] = ikat

    FPGdict['XPfdif'] = ((np.array(psiXo).squeeze() - np.array(psiXu).squeeze() )* 1e-6 * IpiFP).squeeze()
    psi_xpts = np.array( np.column_stack( (psiXo, psiXu) ) )
    psi_xpt_max = np.amax(psi_xpts,axis=1)

    FPGdict['fXP1fIL'] = (np.array(iflim_max).squeeze() - psi_xpt_max) * 1e-6 * IpiFP.squeeze()
    FPGdict['fXP1fAL'] = (np.array(oflim_max).squeeze() - psi_xpt_max) * 1e-6 * IpiFP.squeeze()
    print(FPGdict.keys())
    
      
    if write_augsf:
      make_fpsfh(list(FPGdict.keys()),FPG_diag,fpcof_path,time.size)
      aug_sfutils.ww.write_sf(shot,FPGdict,'./SFH/',FPG_diag, exp=FPG_experiment)
      #sgroup_write_augsf (FPG, shot, FPG_diag, tb_name='TIMEF', \
      #                    experiment=FPG_experiment, verbose=verbose,
      #                    description='FPG from micdu FPcoefficients')

# end of FPG.py

# ----- if standalone then interprete command line arguments --------------------
if __name__ == '__main__':

  import argparse
  parser = argparse.ArgumentParser( \
        description='Evaluate scalar equilibrium parameters through Function Parametrisation (FP)')

  # input shot file
  parser.add_argument('shot',type=int, help='AUG shot number')

  parser.add_argument('-d', '--diag', dest='diag', \
                      action='store', default='MAP', \
                      help='input shotfile diagnostic code (MAP,MAX,MAY,MAG,...)')
  parser.add_argument('-exp', '--experiment', dest='experiment', \
                      action='store', default='AUGD', \
                      help='input shotfile experiment code (AUGD, self, etc.)')
  parser.add_argument('-ed', '--edition', dest='edition', \
                      type=int, action='store', default=0, \
                      help='input shotfile edition number')

  # output shot file
  parser.add_argument('-fsh', '--fpg-shot', dest='FPG_shot', action='store', \
                      type=int, help='shot number for shot file output (skip to use input shot number)')
  parser.add_argument('-fd', '--fpg-diag', dest='FPG_diag', \
                      action='store', default='FPG', \
                      help='output shotfile diagnostic code (EQI,EQH,EQB,IDE,...)')
  parser.add_argument('-fexp', '--fpg-experiment', dest='FPG_experiment', \
                      action='store', default='AUGD', \
                      help='output shotfile experiment code (AUGD, self, etc.)')


  # Function Parametrisation options
  # path to netCDF file with default FP coefficients
  #fpcof_path = '/shares/departments/AUG/users/wls/AUG/FP_coefficients/current.nc'
  fpcof_path = '/shares/departments/AUG/users/micdu/Python/FPregression/FP_coefficients/fpcof_2024A_md2024Dec01_nodpsi02.nc'
  parser.add_argument('-fpc', '--fpcof-path', dest='fpcof_path', action='store', \
                      type=str, default=fpcof_path, help='path to nc file with FP coefficients')
    
  # misc options
  parser.add_argument('-v', '--verbose', dest='verbose', action="store_true",
                      help="print progress messages (not only errors)")
  parser.add_argument('-nowriteSF', '--NowriteSF', dest='writeSF', action="store_false",
                      help="print progress messages (not only errors)")
# -------------- parse arguments ----------------------------------------------
  args = parser.parse_args()


  makeFPG(args.shot,MAG_diag=args.diag,MAG_experiment=args.experiment,MAG_edition=args.edition,
          FPG_diag=args.FPG_diag,FPG_experiment=args.FPG_experiment,
          fpcof_path=args.fpcof_path,verbose=args.verbose,write_augsf=args.writeSF)
