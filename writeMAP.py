import aug_sfutils as dd
import numpy as np
from utils.sgroup_augww import sgroup_write_augsf
from utils.ReadMagData import dataset,ReadBT

class SignalGroup:
    def __init__(self,snames, data, timebase):
        self.snames = snames
        self.data = data
        self.timebase = timebase


    # return signal array for given list of names
    def by_names(self, snames):
        first = True
        for j, sname in enumerate(snames):
            if sname not in self.snames:
                print('Cannot find signal >'+sname+'<')
                continue
        nset = (self.snames == sname)
        for i,b in enumerate(nset):
            if b:
                if first:
                    sigs = self.data[:,i]
                    first = False
                else:
                    sigs = np.column_stack((sigs, self.data[:,i]))
        return sigs

def writeMAP(shot,
             expR='AUGD',diagR='MAY',
             expW='AUGD',diagW='MAP',
             Plot=False,write=True):

    #root = tree.getroot()

    sigs = dataset()
    sigs_map = dataset()


    bt,tbt,tbref,bref,t2 = ReadBT(shot)
    t1 = 0.
    dt = np.mean(np.diff(tbt))
    time = np.arange(t1,t2,dt)
    
    #Read Bpol probe data
    #bpol.names = root.findall("./magdiag/bpol_probes/setup_bprobe/name")[0].text.replace('\n',"").replace("'","").replace(" ","").split(',')
    names = ['Dpsi01D','Dpsi02D','Dpsi03D','Dpsi04D','Dpsi05D','Dpsi06D','Dpsi07D','Dpsi08D','Dpsi09D','Dpsi10D','Dpsi11D',
             'Dpsi12D','Dpsi13D','Dpsi14D','Dpsi15D','Dpsi16D','dpsisu1','dpsiso1',
             'Bthe01','Bthe02','Bthe03','Bthe04','Bthe05','Bthe06','Bthe07','Bthe08','Bthe09','Bthe10', 'Bthe11','Bthe12',
             'Bthe13','Bthe14','Bthe15','Bthe16','Bthe18','Bthe19','Bthe20', 'Bthe21','Bthe22','Bthe24','Bthe25','Bthe26',
             'Bthe27','Bthe28','Bthe29','Bthe30', 'Bthe31','Bthe32','Bthe33','Bthe34','Bthe35','Bthe36','Bthe37','Bthe38',
             'Bthe39','Bthe40','Br7','Br8','Br9','Bhschild','Breten-i','Broof']
    
    '''
    if shot > 41653:
        names = ['Dpsi01D','Dpsi02D','Dpsi03D','Dpsi04D','Dpsi05D','Dpsi06D','Dpsi07D','Dpsi08D','Dpsi09D','Dpsi10D','Dpsi11D',
                 'Dpsi12D','Dpsi13D','Dpsi14D','Dpsi15D','Dpsi16D','dpsisu1',
                 'Bthe01','Bthe02','Bthe03','Bthe04','Bthe05','Bthe06','Bthe07','Bthe08','Bthe09','Bthe10', 'Bthe11','Bthe12',
                 'Bthe13','Bthe14','Bthe15','Bthe16','Bthe18','Bthe19','Bthe20', 'Bthe21','Bthe22','Bthe24','Bthe25','Bthe26',
                 'Bthe27','Bthe28','Bthe29','Bthe30', 'Bthe31','Bthe32','Bthe33','Bthe34','Bthe35','Bthe36','Bthe37','Bthe38',
                 'Bthe39','Bthe40','Br7','Br8','Br9','Bhschild','Breten-i','Broof','Bp03DOi','Bp03DOm',
                 'Bp03DOa','Bp07DOi','Bp07DOm','Bp07DOa','Bp11DOi','Bp11DOm','Bp11DOa','Bp15DOi','Bp15DOm','Bp15DOa',
                 'Br03DOi','Br03DOm','Br03DOa','Br07DOi','Br07DOm','Br07DOa','Br11DOi','Br11DOm','Br11DOa','Br15DOi','Br15DOm','Br15DOa',
                 'Bt03DOi','Bt03DOm','Bt03DOa','Bt07DOi','Bt07DOm','Bt07DOa','Bt11DOi','Bt11DOm','Bt11DOa','Bt15DOi','Bt15DOm','Bt15DOa']
    '''
    namesR = names
    if diagR == 'MAE':
        namesR = ['Dpsi01D','Dpsi02D','Dpsi03D','Dpsi04D','Dpsi05D','Dpsi06D','Dpsi07D','Dpsi08D','Dpsi09D','Dpsi10D','Dpsi11D',
                  'Dpsi12D','Dpsi13D','Dpsi14D','Dpsi15D','Dpsi16D','dpsisu1',
                  'Bp15i01','Bp15i02','Bp15i03','Bp15i04','Bp15i05','Bp15i06','Bp15i07','Bp15i08','Bp15i09','Bp15i10', 'Bp15i11',
                  'Bp15i12','Bp15i13','Bp15i14','Bp15i15','Bp15i16','Bp15i18','Bp15i19','Bp15i20', 'Bp15i21','Bp15i22','Bp15i24',
                  'Bp15i25','Bp15i26','Bp15i27','Bthe28','Bp15i29','Bthe30', 'Bp15i31','Bp15i32','Bp15i33','Bp15i34','Bp15i35',
                  'Bp15i36','Bp15i37','Bp15i38','Bp15i39','Bp15i40','Br7','Br8','Br9','Bhschild','Breten-i','Broof']

    pslnames = ['Ipslon','Ipslun','Ipslok','Ipsluk','BTF']
    #Read Btheta,Dpsi data 
    sigs.names = namesR
    sigs.snames = namesR
    sigs.btcorrect = True
    sigs.ReadData(shot,bt=bt,tbt=tbt,tin=time,bref=bref,tbref=tbref)

    #Read PSL & BT data
    sigs.names = pslnames
    sigs.snames.extend(pslnames)
    sigs.btcorrect = False
    sigs.ReadData(shot,tin=time)

    sigs.data = np.array(sigs.data).transpose()
    sigs.timebase = sigs.times
    print(sigs.snames)

    if write:
        tb_name = 'timeP'
        sgroup_write_augsf(sigs,shot,diagW,tb_name=tb_name,
                           experiment=expW)
        


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('shot',type=int)
    args = parser.parse_args()

    writeMAP(args.shot)
