#!/bin/python

'''
  cliste_eqres.py
  read pjm's FP equilibrium database of 21-Nov-2023
  extend to read interpretive CLISTE results including input and fit data
'''

import numpy as np
import re     # for a more flexible string split()



class cliste_eqres(object):
    '''
      class to read CLISTE eqres file
    '''

    def __init__ (self, fn, n_equil=0,Plot=True):
        '''
          read eqres file 'fn'

          nequil:    0: read all equilibira in eqres file
                    >0: specifiy number of equilibria to read from eqdsk file

        '''
        self.vars = {}
        self.NEQ = 0
        print('Reading eqres')
        # --------------------------- header --------------------------------
        f = open(fn, 'r')
        '''
        l1 = f.readline()
        if not 'CLISTE' in l1:
            print("First line should contain CLISTE keyword but doesn't")
        l1 = f.readline()  # MD file time stamp
        l1 = f.readline()  # comment line
        '''
        l1=''
        while not 'IPLOT' in l1:
            l1 = f.readline()  # IPLOT,NLI,NLO,SSCAL xcalfac IWT ITES8, CYCSLIPFF/SOL, IPOWPFF/SOL. wc0pr
        l1 = f.readline()  # RZMI,RZMA,ZMI,ZMA,ERROR
        l = l1.split()
        self.RZMI = float(l[0])
        self.RZMA = float(l[1])
        self.ZMI = float(l[2])
        self.ZMA = float(l[3])

        l1 = f.readline()  # R, Z GRID UNITS
        l = l1.split()
        self.NR = int(l[0])
        self.NZ = int(l[1])

        l1 = f.readline()  # NPG & IPG ARRAY
        l1 = f.readline()  # ISKP, IAPPEND, npfcondex, ipfcondex
        l1 = f.readline()  # IPLIMS (MA). IW (1=LK 2=WEIN 4=NUM ), IWFAM, ICPROF
        l1 = f.readline()  # PYAGD  MINIMA
        l1 = f.readline()  # PYAGD  MAXIMA
        l1 = f.readline()  # FYAGD  MINIMA
        l1 = f.readline()  # FYAGD  MAXIMA
        l1 = f.readline()  # boopa boopb boofa boofb
        l1 = f.readline()  # same ?
        l1 = f.readline()  # EQ RANF SEED
        while not 'N_EQUIL' in l1:
            l1 = f.readline()
        l = l1.split()
        self.NEQ = int(l[1])   # number of equilibria in this file
        if n_equil!=0 and n_equil<self.NEQ:
            self.NEQ = n_equil

        print("%d equilibria, %dx%d" % (self.NEQ, self.NR+1, self.NZ+1))
        l2 = l1

        # ---------------------------- equilibrium -------------------------
        self.Dpsi_meas = []
        self.Dpsi_equil = []
        self.Btheta_meas = []
        self.Btheta_equil = []
        self.Icoil_meas = []
        self.Icoil_equil = []
        
        self.Pressure_constraint = False
        self.Pressure_rp = []
        self.Pressure_meas = []
        self.Pressure_equil = []
        self.Temp_meas = []
        for ieq in range(self.NEQ):
            #print(ieq)
            # advance to first/next equilibrium
            #while not 'Shafranov' in l2:
            #    l2 = f.readline()
            # skip Shafranov line integrals
            #l2 = f.readline()
            '''
            # advance to output summary
            while not 'CLISTE_p' in l2:
                l2 = f.readline()
            l = l2.split()
            self.add_vars('eqno', ieq, int(l[2]), dtype=np.int32)
            self.add_vars('ikCAT', ieq, int(l[4]), dtype=np.int32)
            self.add_vars('isfCAT', ieq, int(l[-1]), dtype=np.int32)

            print("Eq No. %d (%d), ikCAT=%d, SF KAT = %d" % (ieq+1, int(l[2]), int(l[4]), int(l[-1])))
            '''
            #advance to magnetic fitting
            while not 'Ya' or 'CLI_pred' in l2:
                l2=f.readline()
            if 'Ya' in l2:
                self.interp = True
                l=l2.split()
                self.add_vars('Time', ieq, float(l[2]))
                self.add_vars('dFluxloops', ieq, float(l[6]))
                self.add_vars('dBthetaprobes', ieq, float(l[9]))
            
                l2=f.readline() #skip header
                #Read first line of magnetics
                l2=f.readline()
                l=l2.split()
                self.Dpsi_meas.append(float(l[3]))
                self.Dpsi_equil.append(float(l[4]))
                while 'psi' in l2:
                    l2=f.readline()
                    l=l2.split()
                    if 'psi' in l2:
                        self.Dpsi_meas.append(float(l[3]))
                        self.Dpsi_equil.append(float(l[4]))
                    
                self.Btheta_meas.append(float(l[3]))
                self.Btheta_equil.append(float(l[4]))
                while 'mT' in l2:
                    l2=f.readline()
                    l=l2.split()
                    if 'mT' in l2:
                        self.Btheta_meas.append(float(l[3]))
                        self.Btheta_equil.append(float(l[4]))

                self.Icoil_meas.append(float(l[3])/1e3)
                self.Icoil_equil.append(float(l[4])/1e3)
                while 'Aws' in l2:
                    l2=f.readline()
                    l=l2.split()
                    if 'Aws' in l2:
                        self.Icoil_meas.append(float(l[3])/1e3)
                        self.Icoil_equil.append(float(l[4])/1e3)
                    
                if 'rhopol' in l2:
                    self.Pressure_constraint = True
                    l2=f.readline()
                    l=l2.split()
                    self.Pressure_rp.append(float(l[9]))
                    self.Pressure_meas.append(float(l[4])/1e3)
                    self.Pressure_equil.append(float(l[5])/1e3)
                    self.Temp_meas.append(float(l[15])/1e3)
                    while 'Pr(' in l2:
                        l2=f.readline()
                        l=l2.split()
                        if 'Pr(' in l2:
                            self.Pressure_rp.append(float(l[9]))
                            self.Pressure_meas.append(float(l[4])/1e3)
                            self.Pressure_equil.append(float(l[5])/1e3)
                            self.Temp_meas.append(float(l[15])/1e3)
            else:
                self.interp = False
                
            while not 'Output FPG Parameters' in l2:
                l2 = f.readline()
            l2 = f.readline()
            l = re.split(',|;|\(|\)', l2)
            self.add_vars('Rmag', ieq, float(l[2]))
            self.add_vars('Zmag', ieq, float(l[3]))
            self.add_vars('Rcurr', ieq, float(l[5]))
            self.add_vars('Zcurr', ieq, float(l[6]))
            self.add_vars('Rgeo', ieq, float(l[8]))
            self.add_vars('Zgeo', ieq, float(l[9]))
            l2 = f.readline()
            l = l2.split()
            self.add_vars('ahor', ieq, float(l[1]))
            self.add_vars('bver', ieq, float(l[3]))
            self.add_vars('k', ieq, float(l[5]))
            self.add_vars('Area', ieq, float(l[11]))
            self.add_vars('Vol', ieq, float(l[13]))
            l2 = f.readline()
            l = l2.split()
            self.add_vars('Rin', ieq, float(l[1]))
            self.add_vars('Raus', ieq, float(l[2]))
            self.add_vars('Zoben', ieq, float(l[4]))
            self.add_vars('Zunten', ieq, float(l[5]))
            l2 = f.readline()
            l2 = f.readline()
            l = l2.split()
            self.add_vars('betpol', ieq, float(l[1]))
            self.add_vars('li', ieq, float(l[3]))
            self.add_vars('q0', ieq, float(l[5]))
            self.add_vars('q95', ieq, float(l[7]))
            self.add_vars('Wmhd', ieq, float(l[9]))
            l2 = f.readline()
            self.add_vars('Rx', ieq, float(l2[4:10]))
            self.add_vars('Zx', ieq, float(l2[11:18]))
            self.add_vars('Fx', ieq, 1e-6*float(l2[26:35]))   # muWb -> Wb
            
            self.add_vars('Rx2', ieq, float(l2[46:52]))
            self.add_vars('Zx2', ieq, float(l2[53:60]))
            self.add_vars('Fx2', ieq, 1e-6*float(l2[67:77]))   # muWb -> Wb
            
            l2 = f.readline()
            self.add_vars('Ril', ieq, float(l2[4:10]))
            self.add_vars('Zil', ieq, float(l2[11:18]))
            self.add_vars('Fil', ieq, 1e-6*float(l2[25:33]))   # muWb -> Wb
            
            self.add_vars('Rol', ieq, float(l2[41:47]))
            self.add_vars('Zol', ieq, float(l2[48:55]))
            self.add_vars('Fol', ieq, 1e-6*float(l2[62:70]))   # muWb -> Wb
        f.close
        print('Finished looppppppppppp')
        if self.interp:
            _dum = len(self.Dpsi_meas)
            _ndpsi = int(_dum/self.NEQ)
            self.nDpsi = _ndpsi
            self.Dpsi_meas = np.array(self.Dpsi_meas).reshape((self.NEQ,_ndpsi))
            self.Dpsi_equil = np.array(self.Dpsi_equil).reshape((self.NEQ,_ndpsi))
            
            _dum = len(self.Btheta_meas)
            _nbtheta = int(_dum/self.NEQ)
            self.nBtheta = _nbtheta
            self.Btheta_meas = np.array(self.Btheta_meas).reshape((self.NEQ,_nbtheta))
            self.Btheta_equil = np.array(self.Btheta_equil).reshape((self.NEQ,_nbtheta))

            _dum = len(self.Icoil_meas)
            _ncoil = int(_dum/self.NEQ)
            self.nIcoil = _ncoil
            self.Icoil_meas = np.array(self.Icoil_meas).reshape((self.NEQ,_ncoil))
            self.Icoil_equil = np.array(self.Icoil_equil).reshape((self.NEQ,_ncoil))

            if self.Pressure_constraint:
                _dum = len(self.Pressure_rp)
                _rad = int(_dum / self.NEQ)
                print(_dum,_rad,self.NEQ)
                self.Pressure_rp = np.array(self.Pressure_rp).reshape((self.NEQ,_rad))
                self.Pressure_meas = np.array(self.Pressure_meas).reshape((self.NEQ,_rad))
                self.Pressure_equil = np.array(self.Pressure_equil).reshape((self.NEQ,_rad))
                self.Temp_meas = np.array(self.Temp_meas).reshape((self.NEQ,_rad))
            
            if Plot:
                print(self.vars['dFluxloops'],self.vars['dBthetaprobes'])
                import matplotlib.pyplot as plt
                color = iter(plt.cm.rainbow(np.linspace(0, 1, self.NEQ)))

                fig_p,(ax_p,ax_t) = plt.subplots(2,sharex=True)
                fig_mag,(ax_dp,ax_bt) = plt.subplots(2,sharex=True)
                for i in range(self.NEQ):
                    c = next(color)
                    ax_p.scatter(self.Pressure_rp[i,:],self.Pressure_meas[i,:],marker="s",c=c)
                    ax_p.scatter(self.Pressure_rp[i,:],self.Pressure_equil[i,:],marker="X",c=c)
                
                    ax_t.plot(self.Pressure_rp[i,:],self.Temp_meas[i,:],c=c)
                    ax_t.scatter(self.Pressure_rp[i,:],self.Temp_meas[i,:],c=c,marker='P')

                    ax_p.vlines(1.0,0.,np.max(self.Pressure_meas))
                    ax_t.vlines(1.0,0.,np.max(self.Temp_meas))
                    ax_t.hlines(0.1,0.,np.max(self.Pressure_rp))

                for i in range(self.nDpsi):
                    ax_dp.plot(self.vars['Time'],self.Dpsi_meas-self.Dpsi_equil)
                for i in range(self.nIcoil):
                    ax_bt.plot(self.vars['Time'],self.Icoil_meas-self.Icoil_equil)
                plt.show()

                

    def add_vars(self, name, index, value, dtype=np.float64):
        if not name in self.vars.keys():
            self.vars[name] = np.zeros(self.NEQ, dtype=dtype)
        self.vars[name][index] = value


# ====================== test main  ==============================

if __name__ == '__main__':

    dir = '/shares/departments/AUG/users/micdu/equil/cliste/results/39/39655/'
    fn = dir + 'result'

    self = cliste_selfes(fn, n_equil=3)
    print(self.vars['dFluxloops'],self.vars['dBthetaprobes'])
    import numpy as np
    import matplotlib.pyplot as plt
    color = iter(plt.cm.rainbow(np.linspace(0, 1, self.NEQ)))

    fig_p,(ax_p,ax_t) = plt.subplots(2,sharex=True)
    fig_mag,(ax_dp,ax_bt) = plt.subplots(2,sharex=True)
    for i in range(self.NEQ):
        c = next(color)
        ax_p.scatter(self.Pressure_rp[i,:],self.Pressure_meas[i,:],marker="s",c=c)
        ax_p.scatter(eqr.Pressure_rp[i,:],eqr.Pressure_equil[i,:],marker="X",c=c)

        ax_t.plot(eqr.Pressure_rp[i,:],eqr.Temp_meas[i,:],c=c)
        ax_t.scatter(eqr.Pressure_rp[i,:],eqr.Temp_meas[i,:],c=c,marker='P')

    ax_p.vlines(1.0,0.,np.max(eqr.Pressure_meas))
    ax_t.vlines(1.0,0.,np.max(eqr.Temp_meas))
    ax_t.hlines(0.1,0.,np.max(eqr.Pressure_rp))
    print('plot 1 done')
    for i in range(eqr.nDpsi):
        ax_dp.plot(eqr.vars['Time'],eqr.Dpsi_meas-eqr.Dpsi_equil)
    for i in range(eqr.nIcoil):
        ax_bt.plot(eqr.vars['Time'],eqr.Icoil_meas-eqr.Icoil_equil)
    plt.show()


